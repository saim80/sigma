//
//  SGSessionManager+iOS.h
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGSessionManager.h"

@interface SGSessionManager (iOS)

- (void)setUpSystemDependency;
- (void)tearDownSystemDependency;

@end
