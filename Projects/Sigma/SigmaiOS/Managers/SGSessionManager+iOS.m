//
//  SGSessionManager+iOS.m
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGSessionManager+iOS.h"

@implementation SGSessionManager (iOS)

- (void)setUpSystemDependency  {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_validateSession)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}
- (void)tearDownSystemDependency {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
