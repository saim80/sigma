//
//  SGURLCacheManager+iOS.h
//  SGApp
//
//  Created by sangwoo.im on 5/8/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGCacheManager.h"

@interface SGCacheManager (iOS)

- (void)setUpSystemDependency;
- (void)tearDownSystemDependency;

@end
