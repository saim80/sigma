//
//  SGURLCacheManager+iOS.m
//  SGApp
//
//  Created by sangwoo.im on 5/8/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGCacheManager+iOS.h"
#import "SGCacheManager_Private.h"
#import <UIKit/UIKit.h>

@implementation SGCacheManager (iOS)

- (void)setUpSystemDependency {
    self.observerToken = [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification
                                                                           object:nil
                                                                            queue:[NSOperationQueue mainQueue]
                                                                       usingBlock:
                          ^(NSNotification *note) {
                              [self performCriticalBlock:^{
                                  [self unloadCache];
                              }];
                          }];
}

- (void)tearDownSystemDependency {
    if (self.observerToken) {
        [[NSNotificationCenter defaultCenter] removeObserver:self.observerToken];
    }
}

@end
