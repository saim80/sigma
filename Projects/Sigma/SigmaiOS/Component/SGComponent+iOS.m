//
//  SGComponent+iOS.m
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGComponent+iOS.h"

@implementation SGComponent (iOS)

@end

//--------------------------------------------------------------------------------------------------------------------------------

@implementation SGAppResponder (Component)

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:continueUserActivity:restorationHandler:)]) {
            opened = [obj application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
            *stop = YES;
        }
    }];
    
    return opened;
}

- (void)application:(UIApplication *)application didDecodeRestorableStateWithCoder:(NSCoder *)coder {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didDecodeRestorableStateWithCoder:)]) {
            [obj application:application didDecodeRestorableStateWithCoder:coder];
        }
    }];
}

- (void)application:(UIApplication *)application didFailToContinueUserActivityWithType:(NSString *)userActivityType error:(NSError *)error {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didFailToContinueUserActivityWithType:error:)]) {
            [obj application:application didFailToContinueUserActivityWithType:userActivityType error:error];
        }
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didFailToRegisterForRemoteNotificationsWithError:)]) {
            [obj application:application didFailToRegisterForRemoteNotificationsWithError:error];
        }
    }];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    __block BOOL success = YES;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didFinishLaunchingWithOptions:)]) {
            success &= [obj application:application didFinishLaunchingWithOptions:launchOptions];
        }
    }];
    
    return success;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didReceiveLocalNotification:)]) {
            [obj application:application didReceiveRemoteNotification:userInfo];
        }
    }];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didRegisterForRemoteNotificationsWithDeviceToken:)]) {
            [obj application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
        }
    }];
}

- (void)application:(UIApplication *)application didUpdateUserActivity:(NSUserActivity *)userActivity {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didUpdateUserActivity:)]) {
            [obj application:application didUpdateUserActivity:userActivity];
        }
    }];
}


- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:handleEventsForBackgroundURLSession:completionHandler:)]) {
            [obj application:application handleEventsForBackgroundURLSession:identifier completionHandler:completionHandler];
        }
    }];
}

- (void)application:(UIApplication *)application handleWatchKitExtensionRequest:(NSDictionary *)userInfo reply:(void (^)(NSDictionary * _Nullable))reply {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:handleWatchKitExtensionRequest:reply:)]) {
            [obj application:application handleWatchKitExtensionRequest:userInfo reply:reply];
        }
    }];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:openURL:options:)]) {
            opened = [obj application:app openURL:url options:options];
            *stop = opened;
        }
    }];
    
    return opened;
}

- (BOOL)application:(UIApplication *)application shouldAllowExtensionPointIdentifier:(NSString *)extensionPointIdentifier {
    __block BOOL allowed = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:shouldAllowExtensionPointIdentifier:)]) {
            allowed = [obj application:application shouldAllowExtensionPointIdentifier:extensionPointIdentifier];
            *stop = allowed;
        }
    }];
    
    return allowed;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder {
    __block BOOL restore = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:shouldRestoreApplicationState:)]) {
            restore = [obj application:application shouldRestoreApplicationState:coder];
            
            *stop = restore;
        }
    }];
    
    return restore;
}

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder {
    __block BOOL save = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:shouldSaveApplicationState:)]) {
            save = [obj application:application shouldSaveApplicationState:coder];
            
            *stop = save;
        }
    }];
    
    return save;
}

- (UIViewController *)application:(UIApplication *)application viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder {
    __block UIViewController *VC = nil;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:viewControllerWithRestorationIdentifierPath:coder:)]) {
            VC = [obj application:application viewControllerWithRestorationIdentifierPath:identifierComponents coder:coder];
        }
    }];
    
    return VC;
}

- (BOOL)application:(UIApplication *)application willContinueUserActivityWithType:(NSString *)userActivityType {
    __block BOOL open = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:willContinueUserActivityWithType:)]) {
            open = [obj application:application willContinueUserActivityWithType:userActivityType];
            *stop = open;
        }
    }];
    
    return open;
}

- (void)application:(UIApplication *)application willEncodeRestorableStateWithCoder:(NSCoder *)coder {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:willEncodeRestorableStateWithCoder:)]) {
            [obj application:application willEncodeRestorableStateWithCoder:coder];
        }
    }];
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    __block BOOL open = YES;
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:willFinishLaunchingWithOptions:)]) {
            open &= [obj application:application willFinishLaunchingWithOptions:launchOptions];
        }
    }];
    
    return open;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidBecomeActive:)]) {
            [obj applicationDidBecomeActive:application];
        }
    }];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidEnterBackground:)]) {
            [obj applicationDidEnterBackground:application];
        }
    }];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidReceiveMemoryWarning:)]) {
            [obj applicationDidReceiveMemoryWarning:application];
        }
    }];
}

- (void)applicationProtectedDataDidBecomeAvailable:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationProtectedDataDidBecomeAvailable:)]) {
            [obj applicationProtectedDataDidBecomeAvailable:application];
        }
    }];
}

- (void)applicationProtectedDataWillBecomeUnavailable:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationProtectedDataWillBecomeUnavailable:)]) {
            [obj applicationProtectedDataWillBecomeUnavailable:application];
        }
    }];
}

- (void)applicationShouldRequestHealthAuthorization:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationShouldRequestHealthAuthorization:)]) {
            [obj applicationShouldRequestHealthAuthorization:application];
        }
    }];
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationSignificantTimeChange:)]) {
            [obj applicationSignificantTimeChange:application];
        }
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillEnterForeground:)]) {
            [obj applicationWillEnterForeground:application];
        }
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillResignActive:)]) {
            [obj applicationWillResignActive:application];
        }
    }];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillTerminate:)]) {
            [obj applicationWillTerminate:application];
        }
    }];
    
    [self removeAllComponents];
}


//--------------------------------------------------------------------------------------------------------------------------------

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IOS

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void (^)())completionHandler {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:handleActionWithIdentifier:forRemoteNotification:withResponseInfo:completionHandler:)]) {
            [obj application:application handleActionWithIdentifier:identifier forRemoteNotification:userInfo withResponseInfo:responseInfo completionHandler:completionHandler];
        }
    }];
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didReceiveLocalNotification:)]) {
            [obj application:application didReceiveLocalNotification:notification];
        }
    }];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didReceiveRemoteNotification:fetchCompletionHandler:)]) {
            [obj application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
        }
    }];
}


- (void)application:(UIApplication *)application didChangeStatusBarOrientation:(UIInterfaceOrientation)oldStatusBarOrientation {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didChangeStatusBarFrame:)]) {
            [obj application:application didChangeStatusBarOrientation:oldStatusBarOrientation];
        }
    }];
}


- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:performActionForShortcutItem:completionHandler:)]) {
            [obj application:application performActionForShortcutItem:shortcutItem completionHandler:completionHandler];
        }
    }];
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:performFetchWithCompletionHandler:)]) {
            [obj application:application performFetchWithCompletionHandler:completionHandler];
        }
    }];
}


- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didChangeStatusBarFrame:)]) {
            [obj application:application didChangeStatusBarFrame:oldStatusBarFrame];
        }
    }];
}

- (void)application:(UIApplication *)application willChangeStatusBarFrame:(CGRect)newStatusBarFrame {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:willChangeStatusBarFrame:)]) {
            [obj application:application willChangeStatusBarFrame:newStatusBarFrame];
        }
    }];
}

- (void)application:(UIApplication *)application willChangeStatusBarOrientation:(UIInterfaceOrientation)newStatusBarOrientation duration:(NSTimeInterval)duration {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:willChangeStatusBarOrientation:duration:)]) {
            [obj application:application willChangeStatusBarOrientation:newStatusBarOrientation duration:duration];
        }
    }];
}


- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didRegisterUserNotificationSettings:)]) {
            [obj application:application didRegisterUserNotificationSettings:notificationSettings];
        }
    }];
}


- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void (^)())completionHandler {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:handleActionWithIdentifier:forLocalNotification:withResponseInfo:completionHandler:)]) {
            [obj application:application handleActionWithIdentifier:identifier forLocalNotification:notification withResponseInfo:responseInfo completionHandler:completionHandler];
        }
    }];
}

#endif

@end
