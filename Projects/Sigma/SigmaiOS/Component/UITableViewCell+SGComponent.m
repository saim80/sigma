//
//  SGComponentTableViewCell.m
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "UITableViewCell+SGComponent.h"
#import "SGComponent.h"
#import "NSObject+SGApp.h"

@implementation UITableViewCell (SGComponent)

+ (NSArray *)swizzledSelectors {
    return @[
             [NSValue valueWithPointer:@selector(prepareForReuse)],
             [NSValue valueWithPointer:@selector(setSelected:animated:)],
             [NSValue valueWithPointer:@selector(setHighlighted:animated:)],
             [NSValue valueWithPointer:@selector(willTransitionToState:)],
             [NSValue valueWithPointer:@selector(didTransitionToState:)]
             ];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleAllSelectors];
    });
}

- (void)sgc_prepareForReuse {
    [self sgc_prepareForReuse];
    
    [self makeActiveComponentsPerformSelector:@selector(prepareForReuse) withBlock:^(id obj) {
        [obj prepareForReuse];
    }];
}

- (void)sgc_setSelected:(BOOL)selected animated:(BOOL)animated {
    [self sgc_setSelected:selected animated:animated];
    
    [self makeActiveComponentsPerformSelector:@selector(setSelected:animated:) withBlock:^(id obj) {
        [obj setSelected:selected animated:animated];
    }];
}

- (void)sgc_setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [self sgc_setHighlighted:highlighted animated:animated];
    
    [self makeActiveComponentsPerformSelector:@selector(setHighlighted:animated:) withBlock:^(id obj) {
        [obj setHighlighted:highlighted animated:animated];
    }];
}

- (void)sgc_setEditing:(BOOL)editing animated:(BOOL)animated {
    [self sgc_setEditing:editing animated:animated];
    
    [self makeActiveComponentsPerformSelector:@selector(setEditing:animated:) withBlock:^(id obj) {
        [obj setEditing:editing animated:animated];
    }];
}

- (void)sgc_willTransitionToState:(UITableViewCellStateMask)state {
    [self sgc_willTransitionToState:state];
    
    [self makeActiveComponentsPerformSelector:@selector(willTransitionToState:) withBlock:^(id obj) {
        [obj willTransitionToState:state];
    }];
}

- (void)sgc_didTransitionToState:(UITableViewCellStateMask)state {
    [self sgc_didTransitionToState:state];
    
    [self makeActiveComponentsPerformSelector:@selector(didTransitionToState:) withBlock:^(id obj) {
        [obj didTransitionToState:state];
    }];
}

@end
