//
//  SGBaseView.m
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "UIView+SGComponent.h"
#import "SGComponent.h"
#import "NSObject+SGApp.h"

@implementation UIView (SGComponent)

+ (NSArray *)swizzledSelectors {
    return @[
             [NSValue valueWithPointer:@selector(drawRect:)],
             [NSValue valueWithPointer:@selector(drawRect:forViewPrintFormatter:)],
             [NSValue valueWithPointer:@selector(updateConstraints)],
             [NSValue valueWithPointer:@selector(layoutSubviews)],
             [NSValue valueWithPointer:@selector(didAddSubview:)],
             [NSValue valueWithPointer:@selector(willRemoveSubview:)],
             [NSValue valueWithPointer:@selector(willMoveToSuperview:)],
             [NSValue valueWithPointer:@selector(didMoveToSuperview)],
             [NSValue valueWithPointer:@selector(willMoveToWindow:)],
             [NSValue valueWithPointer:@selector(didMoveToWindow)],
             [NSValue valueWithPointer:@selector(touchesBegan:withEvent:)],
             [NSValue valueWithPointer:@selector(touchesMoved:withEvent:)],
             [NSValue valueWithPointer:@selector(touchesEnded:withEvent:)],
             [NSValue valueWithPointer:@selector(touchesCancelled:withEvent:)]
             ];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleAllSelectors];
    });
}

- (void)sgc_drawRect:(CGRect)rect {
    [self sgc_drawRect:rect];
    
    [self makeActiveComponentsPerformSelector:@selector(drawRect:) withBlock:^(id obj) {
        [obj drawRect:rect];
    }];
}

- (void)sgc_drawRect:(CGRect)rect forViewPrintFormatter:(UIViewPrintFormatter *)formatter {
    [self sgc_drawRect:rect forViewPrintFormatter:formatter];
    
    [self makeActiveComponentsPerformSelector:@selector(drawRect:forViewPrintFormatter:) withBlock:^(id obj) {
        [obj drawRect:rect forViewPrintFormatter:formatter];
    }];
}

- (void)sgc_updateConstraints {
    [self sgc_updateConstraints];
    
    [self makeActiveComponentsPerformSelector:@selector(updateConstraints) withBlock:^(id obj) {
        [obj updateConstraints];
    }];
}

- (void)sgc_layoutSubviews {
    [self sgc_layoutSubviews];
    
    [self makeActiveComponentsPerformSelector:@selector(layoutSubviews) withBlock:^(id obj) {
        [obj layoutSubviews];
    }];
}

- (void)sgc_didAddSubview:(UIView *)subview {
    [self sgc_didAddSubview:subview];
    
    [self makeActiveComponentsPerformSelector:@selector(didAddSubview:) withBlock:^(id obj) {
        [obj didAddSubview:subview];
    }];
}

- (void)sgc_willRemoveSubview:(UIView *)subview {
    [self sgc_willRemoveSubview:subview];
    
    [self makeActiveComponentsPerformSelector:@selector(willRemoveSubview:) withBlock:^(id obj) {
        [obj willRemoveSubview:subview];
    }];
}

- (void)sgc_willMoveToSuperview:(UIView *)newSuperview {
    [self sgc_willMoveToSuperview:newSuperview];
    
    [self makeActiveComponentsPerformSelector:@selector(willMoveToSuperview:) withBlock:^(id obj) {
        [obj willMoveToSuperview:newSuperview];
    }];
}

- (void)sgc_didMoveToSuperview {
    [self sgc_didMoveToSuperview];
    
    [self makeActiveComponentsPerformSelector:@selector(didMoveToSuperview) withBlock:^(id obj) {
        [obj didMoveToSuperview];
    }];
}

- (void)sgc_willMoveToWindow:(UIWindow *)newWindow {
    [self sgc_willMoveToWindow:newWindow];
    
    [self makeActiveComponentsPerformSelector:@selector(willMoveToWindow:) withBlock:^(id obj) {
        [obj willMoveToWindow:newWindow];
    }];
}

- (void)sgc_didMoveToWindow {
    [self sgc_didMoveToWindow];
    
    [self makeActiveComponentsPerformSelector:@selector(didMoveToWindow) withBlock:^(id obj) {
        [obj didMoveToWindow];
    }];
}

- (void)sgc_touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self sgc_touchesBegan:touches withEvent:event];
    
    [self makeActiveComponentsPerformSelector:@selector(touchesBegan:withEvent:) withBlock:^(id obj) {
        [obj touchesBegan:touches withEvent:event];
    }];
}

- (void)sgc_touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self sgc_touchesMoved:touches withEvent:event];
    
    [self makeActiveComponentsPerformSelector:@selector(touchesMoved:withEvent:) withBlock:^(id obj) {
        [obj touchesMoved:touches withEvent:event];
    }];
}

- (void)sgc_touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self sgc_touchesEnded:touches withEvent:event];
    
    [self makeActiveComponentsPerformSelector:@selector(touchesEnded:withEvent:) withBlock:^(id obj) {
        [obj touchesEnded:touches withEvent:event];
    }];
}

- (void)sgc_touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self sgc_touchesCancelled:touches withEvent:event];
    
    [self makeActiveComponentsPerformSelector:@selector(touchesCancelled:withEvent:) withBlock:^(id obj) {
        [obj touchesCancelled:touches withEvent:event];
    }];
}

@end
