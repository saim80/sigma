//
//  SGComponentCollectionViewCell.m
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "UICollectionReusableView+SGComponent.h"
#import "SGComponent.h"
#import "NSObject+SGApp.h"

@implementation UICollectionReusableView (SGComponent)

+ (NSArray *)swizzledSelectors {
    return @[
       [NSValue valueWithPointer:@selector(prepareForReuse)],
       [NSValue valueWithPointer:@selector(preferredLayoutAttributesFittingAttributes:)],
       [NSValue valueWithPointer:@selector(applyLayoutAttributes:)],
       [NSValue valueWithPointer:@selector(willTransitionToState:)],
       [NSValue valueWithPointer:@selector(didTransitionToState:)]
       ];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleAllSelectors];
    });
}

- (void)sgc_prepareForReuse {
    [self sgc_prepareForReuse];
    
    [self makeActiveComponentsPerformSelector:@selector(prepareForReuse) withBlock:^(id obj) {
        [obj prepareForReuse];
    }];
}

- (UICollectionViewLayoutAttributes *)sgc_preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    __block UICollectionViewLayoutAttributes *attr = [self sgc_preferredLayoutAttributesFittingAttributes:layoutAttributes];
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(preferredLayoutAttributesFittingAttributes:)]) {
            attr = [obj preferredLayoutAttributesFittingAttributes:layoutAttributes];
        }
    }];
    
    return attr;
}

- (void)sgc_applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [self sgc_applyLayoutAttributes:layoutAttributes];
    
    [self makeActiveComponentsPerformSelector:@selector(applyLayoutAttributes:) withBlock:^(id obj) {
        [obj applyLayoutAttributes:layoutAttributes];
    }];
}

- (void)sgc_willTransitionFromLayout:(UICollectionViewLayout *)oldLayout toLayout:(UICollectionViewLayout *)newLayout {
    [self sgc_willTransitionFromLayout:oldLayout toLayout:newLayout];
    
    [self makeActiveComponentsPerformSelector:@selector(willTransitionFromLayout:toLayout:) withBlock:^(id obj) {
        [obj willTransitionFromLayout:oldLayout toLayout:newLayout];
    }];
}

- (void)sgc_didTransitionFromLayout:(UICollectionViewLayout *)oldLayout toLayout:(UICollectionViewLayout *)newLayout {
    [self sgc_didTransitionFromLayout:oldLayout toLayout:newLayout];
    
    [self makeActiveComponentsPerformSelector:@selector(didTransitionFromLayout:toLayout:) withBlock:^(id obj) {
        [obj didTransitionFromLayout:oldLayout toLayout:newLayout];
    }];
}

@end
