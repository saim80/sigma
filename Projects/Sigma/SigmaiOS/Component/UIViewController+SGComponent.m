//
//  UIViewController_Component+iOS.m
//  Sigma
//
//  Created by Sangwoo Im on 1/4/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "UIViewController+SGComponent.h"
#import "NSObject+SGApp.h"
#import "SGComponent.h"

@implementation UIViewController (SGComponent)

+ (NSArray *)swizzledSelectors {
    return @[
             [NSValue valueWithPointer:@selector(viewDidLoad)],
             [NSValue valueWithPointer:@selector(viewWillAppear:)],
             [NSValue valueWithPointer:@selector(viewDidAppear:)],
             [NSValue valueWithPointer:@selector(viewWillDisappear:)],
             [NSValue valueWithPointer:@selector(viewDidDisappear:)],
             [NSValue valueWithPointer:@selector(viewWillLayoutSubviews)],
             [NSValue valueWithPointer:@selector(viewDidLayoutSubviews)],
             [NSValue valueWithPointer:@selector(viewWillTransitionToSize:withTransitionCoordinator:)],
             [NSValue valueWithPointer:@selector(willTransitionToTraitCollection:withTransitionCoordinator:)],
             [NSValue valueWithPointer:@selector(willMoveToParentViewController:)],
             [NSValue valueWithPointer:@selector(didMoveToParentViewController:)]
             ];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleAllSelectors];
    });
}

- (void)sgc_viewDidLoad {
    [self sgc_viewDidLoad];
    [self makeActiveComponentsPerformSelector:@selector(viewDidLoad) withBlock:^(id obj) {
        [obj viewDidLoad];
    }];
}

- (void)sgc_viewWillAppear:(BOOL)animated {
    [self sgc_viewWillAppear:animated];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillAppear:) withBlock:^(id obj) {
        [obj viewWillAppear:animated];
    }];
}

- (void)sgc_viewDidAppear:(BOOL)animated {
    [self sgc_viewDidAppear:animated];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidAppear:) withBlock:^(id obj) {
        [obj viewDidAppear:animated];
    }];
}

- (void)sgc_viewWillDisappear:(BOOL)animated {
    [self sgc_viewWillDisappear:animated];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillDisappear:) withBlock:^(id obj) {
        [obj viewWillDisappear:animated];
    }];
}

- (void)sgc_viewDidDisappear:(BOOL)animated {
    [self sgc_viewDidDisappear:animated];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidDisappear:) withBlock:^(id obj) {
        [obj viewDidDisappear:animated];
    }];
}

- (void)sgc_viewWillLayoutSubviews {
    [self sgc_viewWillLayoutSubviews];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillLayoutSubviews) withBlock:^(id obj) {
        [obj viewWillLayoutSubviews];
    }];
}

- (void)sgc_viewDidLayoutSubviews {
    [self sgc_viewDidLayoutSubviews];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidLayoutSubviews) withBlock:^(id obj) {
        [obj viewDidLayoutSubviews];
    }];
}

- (void)sgc_viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [self sgc_viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillTransitionToSize:withTransitionCoordinator:) withBlock:^(id obj) {
        [obj viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    }];
}

- (void)sgc_willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [self sgc_willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
    
    [self makeActiveComponentsPerformSelector:@selector(willTransitionToTraitCollection:withTransitionCoordinator:) withBlock:^(id obj) {
        [obj willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
    }];
}

- (void)sgc_willMoveToParentViewController:(UIViewController *)parent {
    [self sgc_willMoveToParentViewController:parent];
    
    [self makeActiveComponentsPerformSelector:@selector(willMoveToParentViewController:) withBlock:^(id obj) {
        [obj willMoveToParentViewController:parent];
    }];
    
}

- (void)sgc_didMoveToParentViewController:(UIViewController *)parent {
    [self sgc_didMoveToParentViewController:parent];
    
    [self makeActiveComponentsPerformSelector:@selector(didMoveToParentViewController:) withBlock:^(id obj) {
        [obj didMoveToParentViewController:parent];
    }];
}

@end
