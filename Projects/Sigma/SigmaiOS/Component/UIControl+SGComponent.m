//
//  SGComponentControl.m
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "UIControl+SGComponent.h"
#import "SGComponent.h"
#import "NSObject+SGApp.h"

@implementation UIControl (SGComponent)

+ (NSArray *)swizzledSelectors {
    return @[
             [NSValue valueWithPointer:@selector(sendAction:to:forEvent:)],
             [NSValue valueWithPointer:@selector(beginTrackingWithTouch:withEvent:)],
             [NSValue valueWithPointer:@selector(continueTrackingWithTouch:withEvent:)],
             [NSValue valueWithPointer:@selector(endTrackingWithTouch:withEvent:)],
             [NSValue valueWithPointer:@selector(cancelTrackingWithEvent:)]
             ];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleAllSelectors];
    });
}

- (void)sgc_sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event {
    [self sgc_sendAction:action to:target forEvent:event];
    
    [self makeActiveComponentsPerformSelector:@selector(sendAction:to:forEvent:) withBlock:^(id obj) {
        [obj sendAction:action to:target forEvent:event];
    }];
}

- (BOOL)sgc_beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    __block BOOL shouldBegin = [self sgc_beginTrackingWithTouch:touch withEvent:event];
    
    [self makeActiveComponentsPerformSelector:@selector(beginTrackingWithTouch:withEvent:) withBlock:^(id obj) {
        shouldBegin &= [obj beginTrackingWithTouch:touch withEvent:event];
    }];
    
    return shouldBegin;
}

- (BOOL)sgc_continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    __block BOOL shouldContinue = [self sgc_continueTrackingWithTouch:touch withEvent:event];
    
    [self makeActiveComponentsPerformSelector:@selector(continueTrackingWithTouch:withEvent:) withBlock:^(id obj) {
        shouldContinue &= [obj continueTrackingWithTouch:touch withEvent:event];
    }];
    
    return shouldContinue;
}

- (void)sgc_endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    [self sgc_endTrackingWithTouch:touch withEvent:event];
    
    [self makeActiveComponentsPerformSelector:@selector(endTrackingWithTouch:withEvent:) withBlock:^(id obj) {
        [obj endTrackingWithTouch:touch withEvent:event];
    }];
}

- (void)sgc_cancelTrackingWithEvent:(UIEvent *)event {
    [self sgc_cancelTrackingWithEvent:event];
    
    [self makeActiveComponentsPerformSelector:@selector(cancelTrackingWithEvent:) withBlock:^(id obj) {
        [obj cancelTrackingWithEvent:event];
    }];
}

@end
