//
//  SGComponent+iOS.h
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGComponent.h"
#import "SGAppResponder+iOS.h"

@interface SGComponent (iOS)

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGAppResponder (Component)

@end