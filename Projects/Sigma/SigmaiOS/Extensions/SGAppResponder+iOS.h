//
//  SGAppResponder+iOS.h
//  SGApp
//
//  Created by Sangwoo Im on 4/3/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGAppResponder : UIResponder <UIApplicationDelegate>

@end
