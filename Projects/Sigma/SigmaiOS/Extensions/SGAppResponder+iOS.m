//
//  SGAppResponder+iOS.m
//  SGApp
//
//  Created by Sangwoo Im on 4/3/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGAppResponder+iOS.h"
#import "SGFoundation.h"

@interface SGAppResponder ()

@end

@implementation SGAppResponder

- (id)init {
    self = [super init];
    
    if (self) {
        [self _simulateMemoryWarningIfNecessary];
    }
    
    return self;
}

- (void)_simulateMemoryWarningIfNecessary {
#if TARGET_IPHONE_SIMULATOR
    UIApplication * const app = [UIApplication sharedApplication];
    if ([app respondsToSelector:@selector(_performMemoryWarning)]) {
        // every 5 sec, 30% chance to post memory warnings.
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            if (random() % 3 == 0 && [app applicationState] == UIApplicationStateActive) {
                // XXX: This is a private API. must not used for device or actual product.
                [app performSelector:@selector(_performMemoryWarning)];
                SGLog(@"%@", @"Memory Warning Simulated.");
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self _simulateMemoryWarningIfNecessary];
            });
        });
    }
#endif
}

@end
