//
//  SGSessionManager+Mac.h
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGSessionManager.h"

@interface SGSessionManager (Mac)

- (void)setUpSystemDependency;
- (void)tearDownSystemDependency;

@end
