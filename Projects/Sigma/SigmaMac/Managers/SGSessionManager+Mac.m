//
//  SGSessionManager+Mac.m
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGSessionManager+Mac.h"

@implementation SGSessionManager (Mac)

- (void)setUpSystemDependency  {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_validateSession)
                                                 name:NSApplicationDidBecomeActiveNotification
                                               object:nil];
}
- (void)tearDownSystemDependency {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
