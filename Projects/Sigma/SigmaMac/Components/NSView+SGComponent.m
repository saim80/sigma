//
//  SGComponentView.m
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "NSView+SGComponent.h"
#import "NSObject+SGApp.h"
#import "SGComponent.h"

@implementation NSView (SGComponent)

+ (NSArray *)swizzledSelectors {
    return @[
             [NSValue valueWithPointer:@selector(windowWillLoad)],
             [NSValue valueWithPointer:@selector(windowDidLoad)]
             ];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleAllSelectors];
    });
}

- (void)sgc_drawRect:(NSRect)dirtyRect {
    [self sgc_drawRect:dirtyRect];
    
    [self makeActiveComponentsPerformSelector:@selector(drawRect:) withBlock:^(id obj) {
        [obj drawRect:dirtyRect];
    }];
}

- (void)sgc_willRemoveSubview:(NSView *)subview {
    [self sgc_willRemoveSubview:subview];
    
    [self makeActiveComponentsPerformSelector:@selector(willRemoveSubview:) withBlock:^(id obj) {
        [obj willRemoveSubview:subview];
    }];
}

- (void)sgc_didAddSubview:(NSView *)subview {
    [self sgc_didAddSubview:subview];
    
    [self makeActiveComponentsPerformSelector:@selector(didAddSubview:) withBlock:^(id obj) {
        [obj didAddSubview:subview];
    }];
}

- (void)sgc_viewWillDraw {
    [self sgc_viewWillDraw];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillDraw) withBlock:^(id obj) {
        [obj viewWillDraw];
    }];
}

- (void)sgc_viewWillMoveToSuperview:(NSView *)newSuperview {
    [self sgc_viewWillMoveToSuperview:newSuperview];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillMoveToSuperview:) withBlock:^(id obj) {
        [obj viewWillMoveToSuperview:newSuperview];
    }];
}

- (void)sgc_viewWillMoveToWindow:(NSWindow *)newWindow {
    [self sgc_viewWillMoveToWindow:newWindow];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillMoveToWindow:) withBlock:^(id obj) {
        [obj viewWillMoveToWindow:newWindow];
    }];
}

- (void)sgc_viewWillStartLiveResize {
    [self sgc_viewWillStartLiveResize];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillStartLiveResize) withBlock:^(id obj) {
        [obj viewWillStartLiveResize];
    }];
}

- (void)sgc_viewDidChangeBackingProperties {
    [self sgc_viewDidChangeBackingProperties];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidChangeBackingProperties) withBlock:^(id obj) {
        [obj viewDidChangeBackingProperties];
    }];
}

- (void)sgc_viewDidEndLiveResize {
    [self sgc_viewDidEndLiveResize];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidEndLiveResize) withBlock:^(id obj) {
        [obj viewDidEndLiveResize];
    }];
}

- (void)sgc_viewDidHide {
    [self sgc_viewDidHide];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidHide) withBlock:^(id obj) {
        [obj viewDidHide];
    }];
}

- (void)sgc_viewDidMoveToSuperview {
    [self sgc_viewDidMoveToSuperview];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidMoveToSuperview) withBlock:^(id obj) {
        [obj viewDidMoveToSuperview];
    }];
}

- (void)sgc_viewDidMoveToWindow {
    [self sgc_viewDidMoveToWindow];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidMoveToWindow) withBlock:^(id obj) {
        [obj viewDidMoveToWindow];
    }];
}

- (void)sgc_viewDidUnhide {
    [self sgc_viewDidUnhide];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidUnhide) withBlock:^(id obj) {
        [obj viewDidUnhide];
    }];
}

- (void)sgc_updateConstraints {
    [self sgc_updateConstraints];
    
    [self makeActiveComponentsPerformSelector:@selector(updateConstraints) withBlock:^(id obj) {
        [obj updateConstraints];
    }];
}

@end
