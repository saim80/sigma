//
//  SGComponent+Mac.h
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGComponent.h"

@interface SGComponent (Mac)

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGAppResponder : NSObject <NSApplicationDelegate>

@end