//
//  SGComponentWindowController.m
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "NSWindowController+SGComponent.h"
#import "SGComponent.h"
#import "NSObject+SGApp.h"

@implementation NSWindowController (SGComponent)

+ (NSArray *)swizzledSelectors {
    return @[
             [NSValue valueWithPointer:@selector(windowWillLoad)],
             [NSValue valueWithPointer:@selector(windowDidLoad)]
             ];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleAllSelectors];
    });
}

- (void)sgc_windowWillLoad {
    [self sgc_windowWillLoad];
    
    [self makeActiveComponentsPerformSelector:@selector(windowWillLoad) withBlock:^(id obj) {
        [obj windowWillLoad];
    }];
}

- (void)sgc_windowDidLoad {
    [self sgc_windowDidLoad];
    
    [self makeActiveComponentsPerformSelector:@selector(windowDidLoad) withBlock:^(id obj) {
        [obj windowDidLoad];
    }];
}

@end
