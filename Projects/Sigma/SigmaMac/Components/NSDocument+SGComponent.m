//
//  SGComponentDocument.m
//  Sigma
//
//  Created by Sangwoo Im on 1/15/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "NSDocument+SGComponent.h"
#import "SGComponent.h"
#import "NSObject+SGApp.h"

@implementation NSDocument (SGComponent)

+ (NSArray *)swizzledSelectors {
    return @[
             [NSValue valueWithPointer:@selector(windowControllerWillLoadNib:)],
             [NSValue valueWithPointer:@selector(windowControllerDidLoadNib:)],
             [NSValue valueWithPointer:@selector(writeToURL:ofType:error:)],
             [NSValue valueWithPointer:@selector(readFromURL:ofType:error:)],
             [NSValue valueWithPointer:@selector(isEntireFileLoaded)],
             [NSValue valueWithPointer:@selector(makeWindowControllers)]
             ];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleAllSelectors];
    });
}

- (void)sgc_windowControllerWillLoadNib:(NSWindowController *)windowController {
    [self sgc_windowControllerWillLoadNib:windowController];
    
    [self makeActiveComponentsPerformSelector:@selector(windowControllerWillLoadNib:) withBlock:^(id obj) {
        [obj windowControllerWillLoadNib:windowController];
    }];
}

- (void)sgc_windowControllerDidLoadNib:(NSWindowController *)aController {
    [self sgc_windowControllerDidLoadNib:aController];
    
    [self makeActiveComponentsPerformSelector:@selector(windowControllerDidLoadNib:) withBlock:^(id obj) {
        [obj windowControllerDidLoadNib:aController];
    }];
}

- (BOOL)sgc_writeToURL:(NSURL *)url ofType:(NSString *)typeName error:(NSError * _Nullable __autoreleasing *)outError {
    __block BOOL wrote = [self sgc_writeToURL:url ofType:typeName error:outError];
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(writeToURL:ofType:error:)]) {
            wrote = [obj writeToURL:url ofType:typeName error:outError];
            
            *stop = wrote;
        }
    }];
    
    return wrote;
}

- (BOOL)sgc_readFromURL:(NSURL *)url ofType:(NSString *)typeName error:(NSError * _Nullable __autoreleasing *)outError {
    __block BOOL read = [self sgc_readFromURL:url ofType:typeName error:outError];
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(readFromURL:ofType:error:)]) {
            read = [obj readFromURL:url ofType:typeName error:outError];
            
            *stop = read;
        }
    }];
    
    return read;
}

- (BOOL)sgc_isEntireFileLoaded {
    __block BOOL fileLoaded = [self sgc_isEntireFileLoaded];
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(isEntireFileLoaded)]) {
            fileLoaded = [obj isEntireFileLoaded];
            
            *stop = fileLoaded;
        }
    }];
    
    return fileLoaded;
}

- (void)sgc_makeWindowControllers {
    [self sgc_makeWindowControllers];
    
    [self makeActiveComponentsPerformSelector:@selector(makeWindowControllers) withBlock:^(id obj) {
        [obj makeWindowControllers];
    }];
}


@end
