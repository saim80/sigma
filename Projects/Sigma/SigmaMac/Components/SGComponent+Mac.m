//
//  SGComponent+Mac.m
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGComponent+Mac.h"

@implementation SGComponent (Mac)

@end

//--------------------------------------------------------------------------------------------------------------------------------

@implementation SGAppResponder

- (void)applicationWillFinishLaunching:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillFinishLaunching:)]) {
            [obj applicationWillFinishLaunching:notification];
        }
    }];
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidFinishLaunching:)]) {
            [obj applicationDidFinishLaunching:notification];
        }
    }];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    __block NSApplicationTerminateReply outReply = NSTerminateNow;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationShouldTerminate:)]) {
            outReply = [obj applicationShouldTerminate:sender];
            *stop = YES;
        }
    }];
    
    return outReply;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender {
    __block BOOL terminate = YES;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationShouldTerminateAfterLastWindowClosed:)]) {
            terminate = [obj applicationShouldTerminateAfterLastWindowClosed:sender];
            *stop = YES;
        }
    }];
    
    return terminate;
}

- (void)applicationWillTerminate:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillTerminate:)]) {
            [obj applicationWillTerminate:notification];
        }
    }];
    
    [self removeAllComponents];
}

- (void)applicationWillBecomeActive:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillBecomeActive:)]) {
            [obj applicationWillBecomeActive:notification];
        }
    }];
}

- (void)applicationDidBecomeActive:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidBecomeActive:)]) {
            [obj applicationDidBecomeActive:notification];
        }
    }];
}

- (void)applicationWillResignActive:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillResignActive:)]) {
            [obj applicationWillResignActive:notification];
        }
    }];
}

- (void)applicationDidResignActive:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidResignActive:)]) {
            [obj applicationDidResignActive:notification];
        }
    }];
}

- (void)applicationWillHide:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillHide:)]) {
            [obj applicationWillHide:notification];
        }
    }];
}

- (void)applicationWillUnhide:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillUnhide:)]) {
            [obj applicationWillUnhide:notification];
        }
    }];
}

- (void)applicationDidHide:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidHide:)]) {
            [obj applicationDidHide:notification];
        }
    }];
}

- (void)applicationDidUnhide:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidUnhide:)]) {
            [obj applicationDidUnhide:notification];
        }
    }];
}

- (void)applicationWillUpdate:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationWillUpdate:)]) {
            [obj applicationWillUpdate:notification];
        }
    }];
}

- (void)applicationDidUpdate:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidUpdate:)]) {
            [obj applicationDidUpdate:notification];
        }
    }];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag {
    __block BOOL reopen = YES;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationShouldHandleReopen:hasVisibleWindows:)]) {
            [obj applicationShouldHandleReopen:sender hasVisibleWindows:flag];
        }
    }];
    
    return reopen;
}

- (NSMenu *)applicationDockMenu:(NSApplication *)sender {
    __block NSMenu *dockMenu = nil;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDockMenu:)]) {
            dockMenu = [obj applicationDockMenu:sender];
            
            *stop = YES;
        }
    }];
    
    return dockMenu;
}

- (NSError *)application:(NSApplication *)application willPresentError:(NSError *)error {
    __block NSError *outError = error;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:willPresentError:)]) {
            outError = [obj application:application willPresentError:error];
            *stop = YES;
        }
    }];
    
    return outError;
}

- (void)applicationDidChangeScreenParameters:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidChangeScreenParameters:)]) {
            [obj applicationDidChangeScreenParameters:notification];
        }
    }];
}

- (BOOL)application:(NSApplication *)sender openFile:(NSString *)filename {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:openFile:)]) {
            opened = [obj application:sender openFile:filename];
            
            *stop = YES;
        }
    }];
    
    return opened;
}

- (void)application:(NSApplication *)sender openFiles:(NSArray<NSString *> *)filenames {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:openFiles:)]) {
            [obj application:sender openFiles:filenames];
        }
    }];
}

- (BOOL)application:(id)sender openFileWithoutUI:(NSString *)filename {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:openFileWithoutUI:)]) {
            opened = [obj application:sender openFileWithoutUI:filename];
            
            *stop = YES;
        }
    }];
    
    return opened;
}

- (BOOL)application:(NSApplication *)sender openTempFile:(NSString *)filename {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:openTempFile:)]) {
            opened = [obj application:sender openTempFile:filename];
            
            *stop = YES;
        }
    }];
    
    return opened;
}

- (BOOL)application:(NSApplication *)sender printFile:(NSString *)filename {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:printFile:)]) {
            opened = [obj application:sender printFile:filename];
            
            *stop = YES;
        }
    }];
    
    return opened;
}

- (NSApplicationPrintReply)application:(NSApplication *)application printFiles:(NSArray<NSString *> *)fileNames withSettings:(NSDictionary<NSString *,id> *)printSettings showPrintPanels:(BOOL)showPrintPanels {
    
    __block NSApplicationPrintReply outReply = NSPrintingCancelled;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:printFiles:withSettings:showPrintPanels:)]) {
            outReply = [obj application:application printFiles:fileNames withSettings:printSettings showPrintPanels:showPrintPanels];
            
            *stop = YES;
        }
    }];
    
    return outReply;
}

- (BOOL)application:(NSApplication *)application willContinueUserActivityWithType:(NSString *)userActivityType {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:willContinueUserActivityWithType:)]) {
            opened = [obj application:application willContinueUserActivityWithType:userActivityType];
            
            *stop = YES;
        }
    }];
    
    return opened;
}

- (void)application:(NSApplication *)app willEncodeRestorableState:(NSCoder *)coder {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:willEncodeRestorableState:)]) {
            [obj application:app willEncodeRestorableState:coder];
        }
    }];
}

- (void)application:(NSApplication *)app didDecodeRestorableState:(NSCoder *)coder {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didDecodeRestorableState:)]) {
            [obj application:app didDecodeRestorableState:coder];
        }
    }];
}

- (BOOL)applicationOpenUntitledFile:(NSApplication *)sender {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationOpenUntitledFile:)]) {
            opened = [obj applicationOpenUntitledFile:sender];
            
            *stop = YES;
        }
    }];
    
    return opened;
}

- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationShouldOpenUntitledFile:)]) {
            opened = [obj applicationShouldOpenUntitledFile:sender];
            
            *stop = YES;
        }
    }];
    
    return opened;
}

- (void)application:(NSApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didRegisterForRemoteNotificationsWithDeviceToken:)]) {
            [obj application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
        }
    }];
}

- (void)application:(NSApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didFailToContinueUserActivityWithType:error:)]) {
            [obj application:application didFailToRegisterForRemoteNotificationsWithError:error];
        }
    }];
}

- (void)application:(NSApplication *)application didReceiveRemoteNotification:(NSDictionary<NSString *,id> *)userInfo {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didReceiveRemoteNotification:)]) {
            [obj application:application didReceiveRemoteNotification:userInfo];
        }
    }];
}

- (BOOL)application:(NSApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nonnull))restorationHandler {
    __block BOOL opened = NO;
    
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:continueUserActivity:restorationHandler:)]) {
            opened = [obj application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
            
            *stop = YES;
        }
    }];
    
    return opened;
}

- (void)application:(NSApplication *)application didFailToContinueUserActivityWithType:(NSString *)userActivityType error:(NSError *)error {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didFailToContinueUserActivityWithType:error:)]) {
            [obj application:application didFailToContinueUserActivityWithType:userActivityType error:error];
        }
    }];
}

- (void)application:(NSApplication *)application didUpdateUserActivity:(NSUserActivity *)userActivity {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(application:didUpdateUserActivity:)]) {
            [obj application:application didUpdateUserActivity:userActivity];
        }
    }];
}

- (void)applicationDidChangeOcclusionState:(NSNotification *)notification {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:@selector(applicationDidChangeOcclusionState:)]) {
            [obj applicationDidChangeOcclusionState:notification];
        }
    }];
}

@end