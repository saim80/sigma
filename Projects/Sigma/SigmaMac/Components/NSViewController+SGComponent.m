//
//  SGComponentViewController.m
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "NSViewController+SGComponent.h"
#import "SGComponent.h"
#import "NSObject+SGApp.h"

@implementation NSViewController (SGComponent)

+ (NSArray *)swizzledSelectors {
    return @[
             [NSValue valueWithPointer:@selector(viewDidLoad)],
             [NSValue valueWithPointer:@selector(viewWillAppear)],
             [NSValue valueWithPointer:@selector(viewDidAppear)],
             [NSValue valueWithPointer:@selector(updateViewConstraints)],
             [NSValue valueWithPointer:@selector(viewWillLayout)],
             [NSValue valueWithPointer:@selector(viewDidLayout)],
             [NSValue valueWithPointer:@selector(viewWillDisappear)],
             [NSValue valueWithPointer:@selector(viewDidDisappear)],
             [NSValue valueWithPointer:@selector(viewWillTransitionToSize:)]
             ];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleAllSelectors];
    });
}

- (void)sgc_viewDidLoad {
    [self sgc_viewDidLoad];
    // Do view setup here.
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidLoad) withBlock:^(id obj) {
        [obj viewDidLoad];
    }];
}

- (void)sgc_viewWillAppear {
    [self sgc_viewWillAppear];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillAppear) withBlock:^(id obj) {
        [obj viewWillAppear];
    }];
}

- (void)sgc_viewDidAppear {
    [self sgc_viewDidAppear];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidAppear) withBlock:^(id obj) {
        [obj viewDidAppear];
    }];
}

- (void)sgc_updateViewConstraints {
    [self sgc_updateViewConstraints];
    
    [self makeActiveComponentsPerformSelector:@selector(updateViewConstraints) withBlock:^(id obj) {
        [obj updateViewConstraints];
    }];
}

- (void)sgc_viewWillLayout {
    [self sgc_viewWillLayout];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillLayout) withBlock:^(id obj) {
        [obj viewWillLayout];
    }];
}

- (void)sgc_viewDidLayout {
    [self sgc_viewDidLayout];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidLayout) withBlock:^(id obj) {
        [obj viewDidLayout];
    }];
}

- (void)sgc_viewWillDisappear {
    [self sgc_viewWillDisappear];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillDisappear) withBlock:^(id obj) {
        [obj viewWillDisappear];
    }];
}

- (void)sgc_viewDidDisappear {
    [self sgc_viewDidDisappear];
    
    [self makeActiveComponentsPerformSelector:@selector(viewDidDisappear) withBlock:^(id obj) {
        [obj viewDidDisappear];
    }];
}

- (void)sgc_viewWillTransitionToSize:(NSSize)newSize {
    [self sgc_viewWillTransitionToSize:newSize];
    
    [self makeActiveComponentsPerformSelector:@selector(viewWillTransitionToSize:) withBlock:^(id obj) {
        [obj viewWillTransitionToSize:newSize];
    }];
}

@end
