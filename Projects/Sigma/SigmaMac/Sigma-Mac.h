//
//  SG_App_Mac.h
//  SG App Mac
//
//  Created by Sangwoo Im on 3/10/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGSessionManager+Mac.h"
#import "NSViewController+SGComponent.h"
#import "NSView+SGComponent.h"
#import "NSWindowController+SGComponent.h"
#import "NSWindow+SGComponent.h"
#import "NSDocument+SGComponent.h"
#import "SGComponent+Mac.h"