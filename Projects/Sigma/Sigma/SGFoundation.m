//
//  SGFoundation.m
//  SG App
//
//  Created by Sangwoo Im on 3/10/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGFoundation.h"

NSString * const SGFoundationExceptionLine = @"SGFoundation.exception.line";
NSString * const SGFoundationExceptionFile = @"SGFoundation.exception.file";

#ifdef DEBUG
static NSUInteger SGLogLevel = SGLogLevelDefault;
#else
static NSUInteger SGLogLevel = SGLogLevelFatal;
#endif

@implementation SGFoundation

+ (void)setLogLevel:(NSUInteger)logLevel {
#ifdef DEBUG
    SGLogLevel = logLevel;
#endif
}

+ (NSURL *)documentsURL {
    NSURL * const target = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory
                                                                     inDomain:NSAllDomainsMask
                                                            appropriateForURL:nil
                                                                       create:NO
                                                                        error:NULL];
    return target;
}

+ (NSURL *)applicationURL {
    NSURL * const target = [[NSFileManager defaultManager] URLForDirectory:NSApplicationDirectory
                                                                  inDomain:NSAllDomainsMask
                                                         appropriateForURL:nil
                                                                    create:NO
                                                                     error:NULL];
    return target;
}

+ (NSURL *)applicationSupportURL {
    NSURL * const target = [[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory
                                                                  inDomain:NSUserDomainMask
                                                         appropriateForURL:nil
                                                                    create:YES
                                                                     error:NULL];
    
    return target;
}

+ (NSURL *)cachesURL {
    NSURL * const target = [[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory
                                                                  inDomain:NSUserDomainMask
                                                         appropriateForURL:nil
                                                                    create:YES
                                                                     error:NULL];
    return target;
}

+ (NSURL *)temporaryURL {
    NSURL * const URL = [NSURL fileURLWithPath:NSTemporaryDirectory()];
    
    return URL;
}

+ (NSURL *)replacementURL {
    NSURL * const URL = [[NSFileManager defaultManager] URLForDirectory:NSItemReplacementDirectory
                                                               inDomain:NSUserDomainMask
                                                      appropriateForURL:[self applicationSupportURL]
                                                                 create:YES
                                                                  error:NULL];
    
    return URL;
}

+ (void)raiseException:(SGFoundationException)exception file:(char *)file line:(int)line {
    NSDictionary * const info = @{
                                  SGFoundationExceptionLine : @(line),
                                  SGFoundationExceptionFile : @(file)
                                  };
    [self raiseException:exception info:info];
}

+ (void)raiseException:(SGFoundationException)exception info:(NSDictionary *)info {
    NSString *reason = nil;
    
    switch (exception) {
        case SGFoundationDesignatedInitializerException : {
            reason = NSLocalizedString(@"Default or unintended use of initializer. Use designated initializers.", nil);
            break;
        }
        case SGFoundationAbstractClassInstantiationException : {
            reason = NSLocalizedString(@"Abstract class instantiation is forbidden.", nil);
            break;
        }
        default: {
            reason = NSLocalizedString(@"Interal inconsistency occurred.", nil);
            break;
        }
    }
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:reason
                                 userInfo:info];
}

+ (void)raiseExceptionWithReason:(NSString *)reason file:(char *)file line:(int)line {
    NSDictionary * const info = @{
                                  SGFoundationExceptionLine : @(line),
                                  SGFoundationExceptionFile : @(file)
                                  };
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:reason
                                 userInfo:info];
}

+ (void)logWithFile:(const char *)file line:(unsigned long)line logLevel:(NSUInteger)logLevel format:(NSString *)format, ... {
    if (SGLogLevel <= logLevel) {
        va_list list;
        
        va_start(list, format);
        NSString * inMessage = [[NSString alloc] initWithFormat:format arguments:list];
        va_end(list);
        
        NSString * header = SGNSString(@"Sigma: [%@ %ld]:", [@(file) lastPathComponent], line);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@ %@", header, inMessage);
        });
    }
}

@end
