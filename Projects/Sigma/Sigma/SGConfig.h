//
//  SGConfig.h
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SGObject.h"
#import "SGManager.h"

OBJC_EXTERN NSString * _Nonnull const SGConfigDidChangeValue;
OBJC_EXTERN NSString * _Nonnull const SGConfigKey;
OBJC_EXTERN NSString * _Nonnull const SGConfigValue;

/// Provides an abstract layer for all key value pair configuration needs.
/// Default values are imported from a plist file. dynamic values are set and overriden dynamically.
///
/// You can subclass further to add extra dynamic data layer. e.g., you can add AB test remote values.
@interface SGConfig : SGObject
/// Dictionary to obtain all key value pairs.
@property (nonatomic, readonly, nonnull) NSDictionary *configData;

/// Returns initialized data with the default values that are imported from a plist file located by a given URL.
- (nullable instancetype)initWithContentURL:(nonnull NSURL *)URL;

- (void)resetToDefaults;

- (nullable id)objectForKeyedSubscript:(nonnull id<NSCopying>)key;
- (void)setObject:(nonnull id)obj forKeyedSubscript:(nonnull id<NSCopying>)key;

@end
