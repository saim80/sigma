//
//  SGApp.h
//  SGApp
//
//  Created by Sangwoo Im on 4/5/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#define SGAppVersion 1000000

#import "TargetConditionals.h"

#import "SGObject.h"
#import "SGFoundation.h"

// Managers
#import "SGManager.h"
#import "SGCacheManager.h"

// Extensions
#import "SGZipOperation.h"
#import "SGUnzipOperation.h"
#import "SGBlockOperation.h"
#import "SGOperationQueue.h"

// Extensions/Foundation
#import "SGUUID.h"
#import "NSArray+SGApp.h"
#import "NSFileManager+SGApp.h"
#import "NSObject+SGApp.h"

// Structure
#import "SGTreeNode.h"
#import "SGHalfEdge.h"
#import "SGHalfEdgeFace.h"
#import "SGHalfEdgeMesh.h"

// Others
#import "SGAccount.h"
#import "SGComponent.h"
#import "SGConfig.h"

// platform dependent
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE || TARGET_OS_TV || TARGET_OS_IOS
#import "Sigma-iOS.h"
#elif TARGET_OS_MAC
#import "Sigma-Mac.h"
#endif