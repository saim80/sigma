//
//  SGObject_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGObject.h"
#import "SGManager.h"

@interface SGObject ()
@property (nonatomic, strong) NSMutableDictionary *eventHandlers;

@end
