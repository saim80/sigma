//
//  SGConfig.m
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGConfig_Private.h"

@implementation SGConfig
@dynamic configData;

- (instancetype)initWithContentURL:(NSURL *)URL {
    self = [super init];
    
    static SGManager *__sharedManager;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedManager = [[SGManager alloc] initWithQueue:[SGManager globalQueue]];
    });
    
    if (self) {
        NSDictionary * const configData = [[NSDictionary alloc] initWithContentsOfURL:URL];
        
        if (configData) {
            _constantData = configData;
        } else {
            _constantData = [NSDictionary new];
        }
        
        _dynamicData = [NSDictionary new];
        self.manager = __sharedManager;
    }
    
    return self;
}

- (NSDictionary *)configData {
    NSDictionary *outData;
    NSMutableDictionary * const merged = [self.constantData mutableCopy];
    
    [merged addEntriesFromDictionary:self.dynamicData];
    
    outData = [merged copy];
    
    return outData;
}

- (id)valueForUndefinedKey:(NSString *)key {
    __block id outValue;
    
    [self getValueSafelyWithBlock:^(SGConfig *weakSelf) {
        NSDictionary * const data = weakSelf.configData;
        outValue = data[key];
    }];
    
    return outValue;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    [self setValueSafelyWithBlock:^(SGConfig *weakSelf) {
        NSMutableDictionary * const mutable = [weakSelf.dynamicData mutableCopy];
        mutable[key] = value;
        weakSelf.dynamicData = [mutable copy];
        
        [weakSelf performEvent:SGConfigDidChangeValue sender:weakSelf info:@{SGConfigKey : key, SGConfigValue : value}];
    }];
}

- (id)objectForKeyedSubscript:(id<NSCopying>)key {
    return [self valueForUndefinedKey:(NSString *)key];
}

- (void)setObject:(id)obj forKeyedSubscript:(id<NSCopying>)key {
    [self setValue:obj forUndefinedKey:(NSString *)key];
}

- (void)resetToDefaults {
    [self setValueSafelyWithBlock:^(SGConfig *weakSelf) {
        weakSelf.dynamicData = [NSDictionary new];
    }];
}

@end
