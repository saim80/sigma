//
//  SGNetworkComponent_Private.h
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGNetworkComponent.h"
#import "SGSessionManager.h"
#import <AFNetworking/AFNetworking.h>

@interface SGNetworkMonitor ()
@property (nonatomic, strong, nonnull) SGSessionManager *manager;

- (void)_notifyComponentsForNetworkAvailability;

@end
