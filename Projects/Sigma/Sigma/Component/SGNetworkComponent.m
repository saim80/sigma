//
//  SGNetworkComponent.m
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGNetworkComponent_Private.h"

@implementation SGNetworkMonitor

- (instancetype)initWithSessionManager:(SGSessionManager *)manager {
    self = [super init];
    
    if (self) {
        _manager = manager;
    }
    
    return self;
}

- (void)didActivate {
    [super didActivate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_notifyComponentsForNetworkAvailability)
                                                 name:SGSessionManagerNetworkAvailabilityDidChange
                                               object:_manager];
    [self _notifyComponentsForNetworkAvailability];
}

- (void)willDeactivate {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super willDeactivate];
}

- (void)_notifyComponentsForNetworkAvailability {
    if (!self.active) {
        return;
    }
    [[self.target allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj conformsToProtocol:@protocol(SGNetworkResponder)]) {
            id <SGNetworkResponder> const responder = obj;
            
            if (_manager.networkReachable) {
                [responder networkDidBecomeAvailable];
            } else {
                [responder networkDidBecomeUnavailable];
            }
        }
    }];
}

@end
