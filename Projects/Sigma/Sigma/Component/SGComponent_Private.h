//
//  SGComponent_Private.h
//  
//
//  Created by Sangwoo Im on 1/4/16.
//
//

#import "SGComponent.h"
#import "NSObject+SGApp.h"

@interface SGComponent ()

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGComponentStore ()
@property (nonatomic, strong, nonnull) NSDictionary *activeStore;
@property (nonatomic, strong, nonnull) NSDictionary *selectorStore;

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface NSObject (SGComponent_Private)

- (void)_activateComponent:(nonnull SGComponent *)component;
- (void)_deactivateComponent:(nonnull SGComponent *)component;

@end