//
//  SGNetworkComponent.h
//  Sigma
//
//  Created by Sangwoo Im on 1/14/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGComponent.h"

/// SGNetworkResponder provides an interface to respond to network events. Depending on network condition either of availability
/// method will be invoked when network monitor component becomes active. So, your component sets up a correct network status.
@protocol SGNetworkResponder <NSObject>

/// Invoked when network becomes available.
- (void)networkDidBecomeAvailable;
/// Invoked when network becomes unavailable.
- (void)networkDidBecomeUnavailable;

@end

//--------------------------------------------------------------------------------------------------------------------------------

@class SGSessionManager;

@interface SGNetworkMonitor : SGComponent

- (nonnull instancetype)initWithSessionManager:(nonnull SGSessionManager *)manager;

@end

