//
//  SGComponent.m
//  Sigma
//
//  Created by Sangwoo Im on 1/4/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGComponent_Private.h"

@implementation SGComponent

- (instancetype)init {
    self = [super init];
    
    if (self) {
        _activatedWhenRegistered = YES;
    }
    
    return self;
}

- (void)willMoveToTarget:(NSObject *)target {
    if (_activatedWhenRegistered) {
        _active = target != nil;
        if (target) {
            [self willActivate];
        } else {
            [self willDeactivate];
        }
    }
}

- (void)didMoveToTarget {
    if (_activatedWhenRegistered) {
        if (_target) {
            [self didActivate];
        } else {
            [self didDeactivate];
        }
    }
}

- (void)dealloc {
    [self.target unregisterComponent:self];
}

- (void)setActive:(BOOL)active {
    if (_active != active) {
        if (active) {
            [self willActivate];
        } else {
            [self willDeactivate];
        }
        
        _active = active;
        
        if (active) {
            [self didActivate];
        } else {
            [self didDeactivate];
        }
    }
}

- (void)willActivate {
    
}

- (void)didActivate {
    [_target _activateComponent:self];
}

- (void)willDeactivate {
    [_target _deactivateComponent:self];
}

- (void)didDeactivate {
    
}

@end

//--------------------------------------------------------------------------------------------------------------------------------

@implementation SGComponentStore

+ (instancetype)sharedStore {
    static SGComponentStore *outStore;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        outStore = [SGComponentStore new];
    });
    
    return outStore;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        _store         = [NSDictionary new];
        _activeStore   = [NSDictionary new];
        _selectorStore = [NSDictionary new];
    }
    
    return self;
}

- (NSDictionary *)componentsForObject:(NSObject *)object {
    NSDictionary *outDict;
    
    outDict = _store[@(object.hash).stringValue];
    
    if (!outDict) {
        outDict = [NSDictionary new];
    }
    
    return outDict;
}

- (void)setComponents:(NSDictionary *)components forObject:(NSObject *)object {
    NSMutableDictionary * const mutableStore = [_store mutableCopy];
    
    [mutableStore setValue:components forKey:@(object.hash).stringValue];
    
    _store = [mutableStore copy];
}

- (NSSet *)activeComponentsForObject:(NSObject *)object {
    NSSet *outArray;
    
    outArray = _activeStore[@(object.hash).stringValue];
    
    if (!outArray) {
        outArray = [NSSet new];
    }
    
    return outArray;
}

- (void)setActiveComponents:(NSSet *)components ForObject:(NSObject *)object {
    NSMutableDictionary * const mutableStore = [_activeStore mutableCopy];
    
    [mutableStore setValue:components forKey:@(object.hash).stringValue];
    
    _activeStore = [mutableStore copy];
}

@end

//--------------------------------------------------------------------------------------------------------------------------------

@implementation NSObject (SGComponent)

- (NSDictionary *)componentDictionary {
    return [[SGComponentStore sharedStore] componentsForObject:self];
}

- (SGComponent *)componentByName:(NSString *)name {
    NSDictionary * const dict = [self componentDictionary];
    
    return dict[name];
}

- (BOOL)hasComponentWithName:(NSString *)name {
    return [self componentByName:name] != nil;
}

- (NSArray *)allComponents {
    return [[self componentDictionary] allValues];
}

- (NSArray *)allActiveComponents {
    return [[[SGComponentStore sharedStore] activeComponentsForObject:self] allObjects];
}

- (void)removeAllComponents {
    [[self allComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        SGComponent * const component = obj;
        
        [self unregisterComponent:component];
    }];
}

- (void)registerComponent:(SGComponent *)component {
    NSMutableDictionary * const components = [[self componentDictionary] mutableCopy];
    
    NSString * const name = [NSString stringWithFormat:@"%@-%@", NSStringFromClass([component class]), @(component.hash)];
    
    [component willMoveToTarget:self];
    component.target = self;
    [components setObject:component forKey:name];
    
    [[SGComponentStore sharedStore] setComponents:[components copy] forObject:self];
    [component didMoveToTarget];
}

- (void)unregisterComponent:(SGComponent *)component {
    NSMutableDictionary * const components = [[self componentDictionary] mutableCopy];
    
    NSString * const name = [NSString stringWithFormat:@"%@-%@", NSStringFromClass([component class]), @(component.hash)];
    
    SGComponentStore * const store = [SGComponentStore sharedStore];
    
    [component willMoveToTarget:nil];
    [components removeObjectForKey:name];
    component.target = nil;
    
    if ([components count] > 0) {
        [store setComponents:[components copy] forObject:self];
    } else {
        [store setComponents:nil forObject:self];
    }
    
    [component didMoveToTarget];
}

- (void)_activateComponent:(SGComponent *)component {
    SGComponentStore * const store = [SGComponentStore sharedStore];
    
    NSSet * const activeSet = [store activeComponentsForObject:self];
    
    if (![activeSet containsObject:component]) {
        NSMutableSet * const mutableSet = [activeSet mutableCopy];
        
        [mutableSet addObject:component];
        
        [store setActiveComponents:[mutableSet copy] ForObject:self];
    }
}
- (void)_deactivateComponent:(SGComponent *)component {
    SGComponentStore * const store = [SGComponentStore sharedStore];
    
    NSSet * const activeSet = [store activeComponentsForObject:self];
    
    if ([activeSet containsObject:component]) {
        NSMutableSet * const mutableSet = [activeSet mutableCopy];
        
        [mutableSet removeObject:component];
        
        if ([mutableSet count] > 0) {
            [store setActiveComponents:[mutableSet copy] ForObject:self];
        } else {
            [store setActiveComponents:nil ForObject:self];
        }
    }
}

- (void)makeActiveComponentsPerformSelector:(SEL)action withBlock:(void(^)(id obj))block; {
    [[self allActiveComponents] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj respondsToSelector:action]) {
            if (block) {
                block(obj);
            }
        }
    }];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleForSelector:NSSelectorFromString(@"dealloc") withSelector:@selector(sgc_dealloc)];
    });
}

- (void)sgc_dealloc {
    [self sgc_dealloc];
    
    [self removeAllComponents];
}

@end