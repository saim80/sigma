//
//  SGComponent.h
//  Sigma
//
//  Created by Sangwoo Im on 1/4/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import <Foundation/Foundation.h>

/// Base class for component system.
@interface SGComponent : NSObject

@property (nonatomic, weak) NSObject *target;

@property (nonatomic, assign) BOOL activatedWhenRegistered;
@property (nonatomic, assign) BOOL active;

- (void)willMoveToTarget:(NSObject *)target;
- (void)didMoveToTarget;

- (void)willActivate;
- (void)didActivate;

- (void)willDeactivate;
- (void)didDeactivate;

@end

//--------------------------------------------------------------------------------------------------------------------------------

/// This class provides component collection for any NSObject.
@interface SGComponentStore : NSObject
@property (nonatomic, strong) NSDictionary *store;

+ (instancetype)sharedStore;

- (NSDictionary *)componentsForObject:(NSObject *)object;
- (void)setComponents:(NSDictionary *)components forObject:(NSObject *)object;

- (NSSet *)activeComponentsForObject:(NSObject *)object;
- (void)setActiveComponents:(NSSet *)components ForObject:(NSObject *)object;

@end

//--------------------------------------------------------------------------------------------------------------------------------

/// Extends NSObject for component support.
@interface NSObject (SGComponent)

- (NSDictionary *)componentDictionary;

- (SGComponent *)componentByName:(NSString *)name;
- (BOOL)hasComponentWithName:(NSString *)name;
- (NSArray *)allComponents;
- (NSArray *)allActiveComponents;

- (void)removeAllComponents;

- (void)registerComponent:(SGComponent *)component;
- (void)unregisterComponent:(SGComponent *)component;

- (void)makeActiveComponentsPerformSelector:(SEL)action withBlock:(void(^)(id obj))block;

@end

