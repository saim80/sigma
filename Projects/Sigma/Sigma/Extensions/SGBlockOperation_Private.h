//
//  SGBlockOperation_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 3/22/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGBlockOperation.h"
#import "SGOperationQueue_Private.h"

@interface SGBlockOperation ()
@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;

@end
