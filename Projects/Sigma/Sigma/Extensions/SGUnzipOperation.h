//
//  SGUnzipOperation.h
//  SGApp
//
//  Created by Sangwoo Im on 3/12/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGBlockOperation.h"

OBJC_EXTERN NSString * const SGUnzipOperationZipExtension;

typedef NS_ENUM(NSInteger, SGUnzipErrorCodes) {
    SGUnzipErrorInvalidExtension = -1
};

@protocol SGUnzipOperation <NSObject>
@required
@property (nonatomic, strong) NSURL *unzipSourceURL;
@property (nonatomic, strong) NSURL *unzipDestinationURL;
@property (atomic   , assign) float unzipProgress;

@end

@interface SGUnzipOperation : SGBlockOperation <SGUnzipOperation>
@property (nonatomic, strong) NSURL *unzipSourceURL;
@property (nonatomic, strong) NSURL *unzipDestinationURL;
@property (atomic   , assign) float unzipProgress;

- (instancetype)initWithSourceURL:(NSURL *)aURL destinationURL:(NSURL *)destinationURL
                       completion:(SGCompletionBlock)completionBlock;
+ (SGExecutionBlock)unzipOperationBlock;

@end
