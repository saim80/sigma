//
//  SGOperationQueue.m
//  SGApp
//
//  Created by Sangwoo Im on 3/22/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGOperationQueue_Private.h"

@implementation SGOperationQueue

- (id)initWithMaximumNumberOfOperations:(NSUInteger)maxOps {
    self = [super init];
    if (self) {
        _maxOps = maxOps;
    }
    return self;
}

- (NSUInteger)maximumNumberOfOperations {
    return _maxOps;
}

- (void)_pause {
    if (_sleepDuration > 0.0f) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [super setSuspended:YES];
            _sleepTimer = [NSTimer scheduledTimerWithTimeInterval:_sleepDuration
                                                           target:self
                                                         selector:@selector(_resume)
                                                         userInfo:nil
                                                          repeats:NO];
            
            _sleepTime = [NSDate new];
        });
    }
}
- (void)_resume {
    dispatch_async(dispatch_get_main_queue(), ^{
        [super setSuspended:NO];
        _sleepTimer = nil;
        _sleepTime = nil;
    });
}

- (void)_pauseIfNecessary {
    if ([self _canPauseAutomatically] && [self operationCount] >= _maxOps) {
        [self _pause];
    }
}

- (void)_resumeImmediately {
    dispatch_block_t op = ^{
        if (_sleepTimer) {
            [super setSuspended:NO];
            [_sleepTimer invalidate];
            _sleepTimer = nil;
            _sleepTime = nil;
        }
    };
    
    if ([[NSThread mainThread] isEqual:[NSThread currentThread]]) {
        op();
    } else {
        dispatch_sync(dispatch_get_main_queue(), op);
    }
}

- (BOOL)_canPauseAutomatically {
    return _maxOps > 0;
}

- (void)setSleepDuration:(NSTimeInterval)sleepDuration {
    NSTimeInterval const originalDuration = _sleepDuration;
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([_sleepTimer isValid]) {
            NSTimeInterval const diff = sleepDuration - originalDuration;
            
            NSDate * const now = [NSDate new];
            NSDate * const newFireDate = [[_sleepTimer fireDate] dateByAddingTimeInterval:diff];
            
            if ([now compare:newFireDate] == NSOrderedAscending ) {
                [_sleepTimer setFireDate:newFireDate];
            } else {
                [self _resumeImmediately];
            }
            
        }
    });
    _sleepDuration = sleepDuration;
}

- (void)addOperation:(NSOperation *)op {
    SGBlockOperation *blockOp = (SGBlockOperation *)op;
#if DEBUG
    if (op && ![op isKindOfClass:[SGBlockOperation class]]) {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:NSLocalizedString(@"Only subclasses of SGBlockOperation is allowed.", nil)
                                     userInfo:nil];
    }
#endif
    
    blockOp.queue = self;
    
    [super addOperation:op];
    [self _pauseIfNecessary];
}
- (void)addOperations:(NSArray *)ops waitUntilFinished:(BOOL)wait {
    if (wait) {
        [self _resumeImmediately];
    }

    for (SGBlockOperation *op in ops) {
#if DEBUG
        if (op && ![op isKindOfClass:[SGBlockOperation class]]) {
            @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                           reason:NSLocalizedString(@"Only subclasses of SGBlockOperation is allowed.", nil)
                                         userInfo:nil];
        }
#endif
        op.queue = self;
    }
    
    [super addOperations:ops waitUntilFinished:wait];
}

- (void)addSGExecutionBlock:(SGExecutionBlock)executionBlock completion:(SGCompletionBlock)completion {
    SGBlockOperation * const blockOperation = [SGBlockOperation new];
    
    [blockOperation addSGExecutionBlock:executionBlock];
    [blockOperation setSGCompletionBlock:completion];
    
    [self addOperation:blockOperation];
}

- (void)addOperationWithBlock:(void (^)(void))block {
    SGBlockOperation * const operation = [SGBlockOperation new];
    
    [operation addSGExecutionBlock:^(SGBlockOperation *operation) {
        if (block) {
            block();
        }
    }];
    
    [self addOperation:operation];
}

- (void)waitUntilAllOperationsAreFinished {
    [self _resumeImmediately];
    [super waitUntilAllOperationsAreFinished];
}

@end
