//
//  SGUnzipOperation_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 3/12/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGUnzipOperation.h"
#import "SGFoundation.h"
#import "NSFileManager+SGApp.h"

#import <ZipZap/ZipZap.h>

@interface SGUnzipOperation ()

@end
