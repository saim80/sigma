//
//  SGZipOperation.h
//  SGApp
//
//  Created by Sangwoo Im on 3/22/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGBlockOperation.h"

@protocol SGZipOperation <NSObject>
@property (nonatomic, strong, readonly) NSArray *entryURLs;
@property (nonatomic, strong, readonly) NSURL   *sourceURL;
@property (nonatomic, strong, readonly) NSURL   *destinationURL;

@end

@interface SGZipOperation : SGBlockOperation <SGZipOperation>
@property (nonatomic, strong, readonly) NSArray *entryURLs;
@property (nonatomic, strong, readonly) NSURL   *sourceURL;
@property (nonatomic, strong, readonly) NSURL   *destinationURL;

typedef NS_ENUM(NSInteger, SGZipOperationError) {
    SGZipOperationInvalidZipExtension = -1,
    SGZipOperationStreamCopyFailure   = -2
};

- (instancetype)initWithEntryURLs:(NSArray *)entryURLs sourceURL:(NSURL *)sourceURL destinationURL:(NSURL *)destination;
+ (SGExecutionBlock)zipOperationBlock;

@end
