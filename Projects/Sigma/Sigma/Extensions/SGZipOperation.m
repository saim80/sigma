//
//  SGZipOperation.m
//  SGApp
//
//  Created by Sangwoo Im on 3/22/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGZipOperation_Private.h"

static const NSUInteger SGZipOperationBufferSize = 4096;

@implementation SGZipOperation

- (instancetype)initWithEntryURLs:(NSArray *)entryURLs sourceURL:(NSURL *)sourceURL destinationURL:(NSURL *)destination {
    self = [super init];
    
    if (self) {
        _entryURLs      = entryURLs;
        _sourceURL      = sourceURL;
        _destinationURL = destination;
    }
    
    return self;
}

- (void)start {
    [self addSGExecutionBlock:[[self class] zipOperationBlock]];
    [super start];
}

+ (SGExecutionBlock)zipOperationBlock {
    return ^(SGBlockOperation *op) {
        if (![op isCancelled] && !op.error && [op conformsToProtocol:@protocol(SGZipOperation)]) {
            NSFileManager     * const fileManager    = [NSFileManager defaultManager];
            id <SGZipOperation> const zipOp          = (id)op;
            
            NSMutableArray    * const archiveEntries = [NSMutableArray new];
            NSString          * const dirPath        = [zipOp.sourceURL path];
            
            if (![[zipOp.destinationURL pathExtension] isEqualToString:SGUnzipOperationZipExtension]) {
                op.error = [NSError errorWithDomain:NSStringFromClass([self class])
                                               code:SGZipOperationInvalidZipExtension
                                           userInfo:@{
                                                      NSLocalizedDescriptionKey : NSLocalizedString(@"Invalid zip extension", nil)
                                                      }];
            } else {
                NSError *loadError = nil;
                ZZArchive * const mutableArchive = [ZZArchive archiveWithURL:zipOp.destinationURL
                                                                       error:&loadError];
                
                [zipOp.entryURLs enumerateObjectsUsingBlock:^(NSURL *aURL, NSUInteger idx, BOOL *stop) {
                    BOOL     directoryEntry = NO;
                    NSString *fileName      = nil;
                    
                    NSString * const filePath = [aURL path];
                    
                    if ([fileManager fileExistsAtPath:filePath isDirectory:&directoryEntry] &&
                        [filePath hasPrefix:dirPath]                                        ) {
                        fileName = [filePath stringByReplacingOccurrencesOfString:dirPath withString:@""];
                        
                        if ([fileName hasSuffix:@"/"]) {
                            fileName = [fileName substringFromIndex:1];
                        }
                    }
                    
                    if ([fileName length] > 0) {
                        ZZArchiveEntry *anEntry = nil;
                        
                        if (directoryEntry) {
                            anEntry = [ZZArchiveEntry archiveEntryWithDirectoryName:fileName];
                        } else {
                            anEntry = [ZZArchiveEntry archiveEntryWithFileName:fileName
                                                                      compress:YES
                                                                   streamBlock:
                                       ^BOOL(NSOutputStream * const outStream, NSError *__autoreleasing *error) {
                                           return [[self class] _writeToStream:outStream fromURL:aURL error:error];
                                       }];
                        }
                        
                        [archiveEntries addObject:anEntry];
                    }
                }];
                
                if (!mutableArchive) {
                    op.error = loadError;
                }
            }
        }
    };
}

+ (BOOL)_writeToStream:(NSOutputStream *)outStream fromURL:(NSURL *)URL error:(NSError *__autoreleasing*)error {
    NSInputStream * const inStream = [[NSInputStream alloc] initWithURL:URL];
    
    [outStream open];
    [inStream open];
    
    NSError    *copyError = nil;
    uint8_t    buffer[SGZipOperationBufferSize];
    NSUInteger read = 0;
    
    while ((read = [inStream read:buffer maxLength:SGZipOperationBufferSize]) > 0) {
        NSUInteger written = 0;
        
        if ([outStream hasSpaceAvailable]) {
            written = [outStream write:buffer maxLength:SGZipOperationBufferSize];
        }
        
        if (read != written) {
            NSString * const format      = NSLocalizedString(@"%@: read = %lu, written = %lu", nil);
            NSString * const errorReason = [[NSString alloc] initWithFormat:format, URL, (unsigned long)read, written];
            
            copyError = [NSError errorWithDomain:NSStringFromClass([self class])
                                            code:SGZipOperationStreamCopyFailure
                                        userInfo:@{
                                                   NSLocalizedDescriptionKey        : @"Stream copy failed while zipping.",
                                                   NSLocalizedFailureReasonErrorKey : errorReason
                                                   }];
            break;
        }
    }
    
    [inStream close];
    [outStream close];
    
    if (error) {
        *error = copyError;
    }
    
    return (copyError == nil);
}

@end
