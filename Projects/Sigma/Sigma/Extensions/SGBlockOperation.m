//
//  SGOperation.m
//  SGApp
//
//  Created by Sangwoo Im on 3/22/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGBlockOperation_Private.h"

@implementation SGBlockOperation

- (void)addExecutionBlock:(void (^)(void))block {
    [self addSGExecutionBlock:^(SGBlockOperation *operation) {
        if (block) {
            block();
        }
    }];
}

- (void)setCompletionBlock:(void (^)(void))block {
    [self setSGCompletionBlock:^(SGBlockOperation *operation) {
        if (block) {
            block();
        }
    }];
}

- (void)addSGExecutionBlock:(SGExecutionBlock)block {
    __weak SGBlockOperation * const weakOperation = self;
    [super addExecutionBlock:^{
        if (block) {
            block(weakOperation);
        }
        ++weakOperation.executedBlockCount;
    }];
    ++_totalExpectedBlockCount;
}

- (void)setSGCompletionBlock:(SGCompletionBlock)completion {
    __weak SGBlockOperation * const weakOperation = self;
    [super setCompletionBlock:^{
        weakOperation.endTime = [NSDate new];
        
        if (completion) {
            completion(weakOperation);
        }
        
        if ([weakOperation.queue _canPauseAutomatically]) {
            SGOperationQueue * const queue = weakOperation.queue;
            float ow, nw, m;
            
            switch (queue.adaptionSpeed) {
                case SGOperationQueueAdaptionFast:
                    ow = 0.35f;
                    nw = 0.65f;
                    m  = 1.0f;
                    break;
                case SGOperationQueueAdaptionSlow:
                    ow = 0.5f;
                    nw = 0.5f;
                    m  = 1.12f;
                    break;
                case SGOperationQueueAdaptionNormal:
                default:
                    ow = 0.45f;
                    nw = 0.55f;
                    m  = 1.12f;
                    break;
            }
            
            if (queue.sleepDuration == 0.0f) {
                queue.sleepDuration = weakOperation.runningTime;
            } else {
                queue.sleepDuration = (queue.sleepDuration * ow + weakOperation.runningTime * nw) * queue.maxOps * m;
            }
        }
        
        weakOperation.queue = nil;
    }];
}

- (float)progress {
    float p = 0.0f;
    
    if (_totalExpectedBlockCount > 0) {
        p = self.executedBlockCount / (float)_totalExpectedBlockCount;
    }
    
    return p;
}

- (NSTimeInterval)runningTime {
    NSTimeInterval time = 0.0f;
    
    if (self.startTime) {
        NSDate * const finish = _endTime == nil ? [NSDate new] : _endTime;
        
        time = [finish timeIntervalSinceDate:self.startTime];
    }
    
    return time;
}

- (void)start {
    if (!self.startTime) {
        self.startTime = [NSDate new];
    }
    
    if (!self.completionBlock) {
        [self setSGCompletionBlock:nil];
    }
    
    [super start];
}

@end
