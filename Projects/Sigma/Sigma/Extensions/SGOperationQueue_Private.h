//
//  SGOperationQueue_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 3/22/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGOperationQueue.h"
#import "SGBlockOperation.h"

@interface SGOperationQueue ()
@property (nonatomic, strong) NSTimer        *sleepTimer;
@property (nonatomic, strong) NSDate         *sleepTime;
@property (nonatomic, assign) NSTimeInterval sleepDuration;
@property (nonatomic, assign) NSUInteger     maxOps;

- (BOOL)_canPauseAutomatically;

@end
