//
//  SGZipOperation_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 3/22/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGZipOperation.h"
#import "SGFoundation.h"
#import "SGUnzipOperation.h"

#import <ZipZap/ZipZap.h>

@interface SGZipOperation ()

+ (BOOL)_writeToStream:(NSOutputStream *)outStream fromURL:(NSURL *)URL error:(NSError *__autoreleasing*)error;

@end
