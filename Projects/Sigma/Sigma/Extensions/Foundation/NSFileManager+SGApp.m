//
//  NSFileManager+SGApp.m
//  SGApp
//
//  Created by Sangwoo Im on 3/2/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "NSFileManager+SGApp.h"
#import "SGFoundation.h"
#import "SGUUID.h"

NSString * const SGAppAsyncDeleteDirectory = @"sigma_delete";

@implementation NSFileManager (SGApp)

+ (NSURL *)_asyncDeleteURL {
    NSUUID   * const uuid    = [NSUUID new];
    NSURL    * const tempURL = [SGFoundation temporaryURL];
    NSString * const dirName = [NSString stringWithFormat:@"%@_%@", SGAppAsyncDeleteDirectory, uuid];
    NSURL    * const outURL  = [tempURL URLByAppendingPathComponent:dirName];
    
    return outURL;
}

- (void)removeItemAsynchronouslyAtURL:(NSURL *)aURL completion:(void (^)(NSError *))completion {
    NSError *moveError = nil;
    
    dispatch_queue_t const global = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, (unsigned long)NULL);
    
    if ([aURL checkResourceIsReachableAndReturnError:&moveError]) {
        NSURL * const targetURL = [[self class] _asyncDeleteURL];
        
        if ([self moveItemAtURL:aURL toURL:targetURL error:&moveError]) {
            dispatch_async(global, ^{
                NSError *removeError = nil;
                [self removeItemAtURL:targetURL error:&removeError];
                
                if (completion) {
                    completion(removeError);
                }
            });
        }
    }
    
    if (completion) {
        dispatch_async(global, ^{
            completion(moveError);
        });
    }
}

@end
