//
//  NSArray+SGApp.h
//  SGApp
//
//  Created by Sangwoo Im on 3/10/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface NSArray (SGApp)

+ (id)arrayWithCGPointArray:(CGPoint *)cArray count:(NSUInteger)count;
- (void)getCGPointArray:(CGPoint *)cArray count:(NSUInteger)count;

@end
