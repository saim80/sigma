//
//  NSFileManager+SGApp.h
//  SGApp
//
//  Created by Sangwoo Im on 3/2/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGFoundation_Types.h"

@interface NSFileManager (SGApp)

- (void)removeItemAsynchronouslyAtURL:(NSURL *)aURL completion:(void (^)(NSError *error))completion;

@end
