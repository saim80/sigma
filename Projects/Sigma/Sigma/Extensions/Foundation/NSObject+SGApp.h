//
//  NSObject+Swizzling.h
//  Sigma
//
//  Created by Sangwoo Im on 5/22/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (SGApp)

+ (nullable NSArray *)swizzledSelectors;
+ (void)swizzleForSelector:(nonnull SEL)originalSelector withSelector:(nonnull SEL)swizzledSelector;
+ (void)swizzleAllSelectors;

@end
