//
//  SGUUID.h
//  SGApp
//
//  Created by sangwoo.im on 4/4/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

OBJC_EXTERN NSString * const SGUUIDComponentSeparator;
OBJC_EXTERN NSString * const SGUUIDPrefix;
OBJC_EXTERN NSString * const SGUUIDEscapedPrefix;
OBJC_EXTERN NSString * const SGUUIDRegex;

@interface SGUUID : NSObject <NSCopying>
@property (nonatomic, copy, readonly) NSString *UUIDString;
@property (nonatomic, unsafe_unretained) Class modelClass;

- (id)initWithUUID:(NSUUID *)UUID modelClass:(Class)modelClass;
- (id)initWithUUIDString:(NSString *)string modelClass:(Class)modelClass;
- (id)initWithSGUUIDString:(NSString *)SGUUIDString;
- (NSString *)SGUUIDString;

@end
