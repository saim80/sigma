//
//  NSArray+SGApp.m
//  SGApp
//
//  Created by Sangwoo Im on 3/10/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "NSArray+SGApp.h"

#define CONVERT_TO_CARRAY(DATA_TYPE, RESULT_ARRAY, RESULT_ARRAY_COUNT)  \
                                                                        \
    for (NSUInteger idx = 0; idx < [self count]; idx++) {               \
    typeof(DATA_TYPE) data;                                             \
    NSValue * const dataValue = self[idx];                              \
                                                                        \
    [dataValue getValue:&data];                                         \
        if (RESULT_ARRAY_COUNT > idx) {                                 \
            RESULT_ARRAY[idx] = data;                                   \
        }                                                               \
    }

#define CONVERT_TO_NSARRAY(DATA_TYPE, INPUT_ARRAY, INPUT_COUNT)                      \
                                                                                     \
    NSMutableArray *outArray = [NSMutableArray new];                                 \
    for (NSUInteger idx = 0; idx < INPUT_COUNT; idx++) {                             \
        typeof(DATA_TYPE) const data = INPUT_ARRAY[idx];                             \
        NSValue * const value = [NSValue valueWithBytes:&data                        \
                                               objCType:@encode(typeof(DATA_TYPE))]; \
                                                                                     \
        [outArray addObject:value];                                                  \
    }                                                                                \
                                                                                     \
    return [outArray copy];



@implementation NSArray (SGApp)

+ (id)arrayWithCGPointArray:(CGPoint *)cArray count:(NSUInteger)count {
    CONVERT_TO_NSARRAY(CGPoint, cArray, count);
}

- (void)getCGPointArray:(CGPoint *)cArray count:(NSUInteger)count {
    CONVERT_TO_CARRAY(CGPoint, cArray, count);
}

@end
