//
//  SGUUID.m
//  SGApp
//
//  Created by sangwoo.im on 4/4/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGUUID_Private.h"
#import "SGFoundation.h"

NSString * const SGUUIDComponentSeparator = @"|";
NSString * const SGUUIDPrefix             = @"SGUUID|";
NSString * const SGUUIDEscapedPrefix      = @"\\S\\G\\U\\U\\I\\D\\|";
NSString * const SGUUIDRegex              = @"^SGUUID\\|[a-zA-z]\\w*\\|\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}";

@implementation SGUUID

- (id)init {
    [SGFoundation raiseException:SGFoundationDesignatedInitializerException file:__FILE__ line:__LINE__];
    return nil;
}

- (id)initWithUUID:(NSUUID *)UUID modelClass:(Class)modelClass {
    return [self initWithUUIDString:[UUID UUIDString] modelClass:modelClass];
}

- (id)initWithUUIDString:(NSString *)string modelClass:(Class)modelClass {
    self = [super init];
    
    if (self) {
        _UUIDString = string;
        _modelClass = modelClass;
    }
    
    return self;
}

- (id)initWithSGUUIDString:(NSString *)SGUUIDString {
    NSError * error = nil;
    NSRegularExpression * const regex = [NSRegularExpression regularExpressionWithPattern:SGUUIDRegex
                                                                                  options:0
                                                                                    error:&error];
    // exactly one match.
    if ([[regex matchesInString:SGUUIDString options:0 range:NSMakeRange(0, [SGUUIDString length])] count] == 1) {
        NSString * UUIDString = nil;
        Class      modelClass = NULL;
        
        NSArray  * const IDComponents = [SGUUIDString componentsSeparatedByString:SGUUIDComponentSeparator];
        modelClass = NSClassFromString(IDComponents[1]);
        UUIDString = IDComponents[2];
        
        NSAssert(modelClass != NULL && [UUIDString length] > 0, @"Invalid input arguments for SGUUIDString");
        
        self = [self initWithUUIDString:UUIDString modelClass:modelClass];
    }
    if (error) {
        SGLog(@"%@", error);
    }
    
    return self;
}
- (NSString *)description {
    return _UUIDString;
}
- (NSString *)SGUUIDString {
    return SGNSString(@"%@%@|%@", SGUUIDPrefix, NSStringFromClass(_modelClass), [self UUIDString]);
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] alloc] initWithUUIDString:_UUIDString modelClass:_modelClass];
}

- (BOOL)isEqual:(id)object {
    return _modelClass == [object modelClass] && [[object UUIDString] isEqualToString:_UUIDString];
}

@end
