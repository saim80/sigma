//
//  NSObject+Swizzling.m
//  Sigma
//
//  Created by Sangwoo Im on 5/22/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "NSObject+SGApp.h"
#import <objc/runtime.h>

@implementation NSObject (SGApp)

+ (void)swizzleForSelector:(SEL)originalSelector withSelector:(SEL)swizzledSelector {
    Class class = [self class];
    
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    
    BOOL didAddMethod =
    class_addMethod(class,
                    originalSelector,
                    method_getImplementation(swizzledMethod),
                    method_getTypeEncoding(swizzledMethod));
    
    if (didAddMethod) {
        class_replaceMethod(class,
                            swizzledSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}


+ (NSDictionary *)swizzledSelectors {
    return nil;
}

+ (void)swizzleAllSelectors {
    [[self swizzledSelectors] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        SEL originalSelector;
        SEL swizzledSelector;
        NSString *swizzledName;
        
        [(NSValue *)obj getValue:&originalSelector];
        
        swizzledName     = [NSString stringWithFormat:@"sgc_%@", obj];
        swizzledSelector = NSSelectorFromString(swizzledName);
        
        [self swizzleForSelector:originalSelector withSelector:swizzledSelector];
    }];
}

@end
