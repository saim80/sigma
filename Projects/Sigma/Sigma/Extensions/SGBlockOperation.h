//
//  SGOperation.h
//  SGApp
//
//  Created by Sangwoo Im on 3/22/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGFoundation_Types.h"

@class SGOperationQueue, SGBlockOperation;

@interface SGBlockOperation : NSBlockOperation
@property (nonatomic, strong) id           result;
@property (nonatomic, strong) NSError      *error;
@property (nonatomic, copy)   NSDictionary *userInfo;

@property (nonatomic, assign) NSUInteger totalExpectedBlockCount;
@property (atomic   , assign) NSUInteger executedBlockCount;

@property (nonatomic, readonly) NSTimeInterval runningTime;
@property (nonatomic, readonly) float          progress;

@property (nonatomic, weak) SGOperationQueue *queue;

- (void)addSGExecutionBlock:(SGExecutionBlock)block;
- (void)setSGCompletionBlock:(SGCompletionBlock)block;

@end
