//
//  SGOperationQueue.h
//  SGApp
//
//  Created by Sangwoo Im on 3/22/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGFoundation_Types.h"

typedef NS_ENUM(unsigned short, SGOperationQueueAdaptionSpeed) {
    SGOperationQueueAdaptionNormal,
    SGOperationQueueAdaptionSlow,
    SGOperationQueueAdaptionFast
};

//! Queue with completion block.
//! When maximum number of operations is greater than 0, the queue automatically throttles its operations to ease CPU.
@interface SGOperationQueue : NSOperationQueue
@property (nonatomic, assign, readonly) NSUInteger maximumNumberOfOperations;
@property (nonatomic, assign) SGOperationQueueAdaptionSpeed adaptionSpeed;

//! Returns an instance of queue with given maximum number of operations.
- (id)initWithMaximumNumberOfOperations:(NSUInteger)maxOps;
//! single block operation.
- (void)addSGExecutionBlock:(SGExecutionBlock)executionBlock completion:(SGCompletionBlock)completion;

@end
