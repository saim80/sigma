//
//  SGUnzipOperation.m
//  SGApp
//
//  Created by Sangwoo Im on 3/12/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGUnzipOperation_Private.h"

static const NSUInteger SGUnzipBufferSize = 4096;

NSString * const SGUnzipOperationZipExtension = @"sgz";
NSString * const SGUnzipOperationTmpExtension = @"tmp";

@implementation SGUnzipOperation

+ (NSUInteger)_bufferSize {
    return SGUnzipBufferSize;
}

+ (BOOL)_extractEntry:(ZZArchiveEntry *)entry path:(NSString *)path error:(NSError * __autoreleasing *)entryError_p {
    NSFileManager * const fManager    = [NSFileManager defaultManager];
    NSString      * const tmpFile     = [path stringByAppendingPathComponent:entry.fileName];
    
    BOOL success = YES;
    
    if ([entry.fileName hasSuffix:@"/"] && entry.uncompressedSize == 0) {
        if (![fManager createDirectoryAtPath:tmpFile withIntermediateDirectories:YES attributes:nil error:entryError_p]) {
            success = NO;
        }
    } else {
        NSInputStream  * const inputStream  = [entry newStreamWithError:entryError_p];
        NSOutputStream * const outputStream = [[NSOutputStream alloc] initToFileAtPath:tmpFile
                                                                                append:NO];
        
        if ([fManager fileExistsAtPath:tmpFile]) {
            [fManager removeItemAtPath:tmpFile error:&(*entryError_p)];
        }
        
        [outputStream open];
        [inputStream open];
        
        if ([inputStream hasBytesAvailable]) {
            const NSUInteger maxLength = [[self class] _bufferSize];
            uint8_t buffer[maxLength];
            NSUInteger read = 0;
            
            while ((read = [inputStream read:buffer maxLength:maxLength]) > 0) {
                if ([outputStream hasSpaceAvailable]) {
                    [outputStream write:buffer maxLength:read];
                } else if (entryError_p) {
                    NSString * const errorDescription = @"Unarchive failed. There is no space to unarchive a file.";
                    *entryError_p = [NSError errorWithDomain:NSStringFromClass([self class])
                                                        code:-1
                                                    userInfo:@{ NSLocalizedDescriptionKey : errorDescription }];
                    
                    success = NO;
                    break;
                }
            }
        }
        
        [inputStream close];
        [outputStream close];
    }
    
    return success;
}

+ (SGExecutionBlock)unzipOperationBlock {
    return ^(SGBlockOperation *op) {
        if (![op isCancelled] && !op.error && [op conformsToProtocol:@protocol(SGUnzipOperation)]) {
            id <SGUnzipOperation> const unzipOp   = (id)op;
            NSURL               * const URL       = unzipOp.unzipSourceURL;
            NSString            * const target    = [[NSUUID UUID] UUIDString];
            NSURL               * const tmpURL    = [[SGFoundation temporaryURL] URLByAppendingPathComponent:target];
            NSString            * const extension = [[URL pathExtension] lowercaseString];
            NSFileManager       * const fManager  = [NSFileManager defaultManager];
            
            __block NSError *unzipError = nil;
            
            if ([extension isEqualToString:SGUnzipOperationZipExtension]) {
                NSString * const path = [tmpURL path];
                
                NSError *loadError = nil;
                ZZArchive * const archive = [ZZArchive  archiveWithURL:URL error:&loadError];
                
                if (archive) {
                    if ([fManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&loadError]) {
                        __block NSUInteger unzipped = 0;
                        NSUInteger entryCount = [[archive entries] count];
                        
                        [[archive entries] enumerateObjectsUsingBlock:
                         ^(ZZArchiveEntry * const entry, NSUInteger idx, BOOL *stop) {
                             NSError *entryError = nil;
                             
                             [self _extractEntry:entry path:path error:&entryError];
                             
                             if (entryError) {
                                 unzipError = entryError;
                                 SGError(@"Unarchiving zip file entry failed: %@", entryError);
                                 *stop = YES;
                             } else {
                                 ++unzipped;
                                 unzipOp.unzipProgress = (float)unzipped / (float)entryCount;
                             }
                         }];
                    } else {
                        SGError(@"Couldn't create temporary directory: %@", loadError);
                        unzipError = loadError;
                    }
                } else {
                    unzipError = loadError;
                    SGError(@"Unarchiving zip file: %@", loadError);
                }
            } else {
                unzipError = [NSError errorWithDomain:NSStringFromClass([self class])
                                                 code:SGUnzipErrorInvalidExtension
                                             userInfo:@{ NSLocalizedDescriptionKey : extension }];
                SGError(@"Invalid zip extension: %@", unzipError);
            }
            
            if (!unzipError) {
                __block NSError *moveError = nil;
                
                NSArray * const unzippedContents = [fManager contentsOfDirectoryAtURL:tmpURL
                                                           includingPropertiesForKeys:nil
                                                                              options:0
                                                                                error:&moveError];
                
                if (!moveError) {
                    [unzippedContents enumerateObjectsUsingBlock:^(NSURL *URL, NSUInteger idx, BOOL *stop) {
                        if (![fManager replaceItemAtURL:unzipOp.unzipDestinationURL
                                          withItemAtURL:tmpURL
                                         backupItemName:nil
                                                options:0
                                       resultingItemURL:nil
                                                  error:&moveError]) {
                            SGError(@"Couldn't move unzipped data from (%@) to (%@).", tmpURL, unzipOp.unzipDestinationURL);
                            unzipError = moveError;
                            *stop = YES;
                        }
                    }];
                }
                
                if (!moveError) {
                    op.result = unzipOp.unzipDestinationURL;
                }
            }
            
            if (unzipError) {
                op.error = unzipError;
            }
            
            [fManager removeItemAsynchronouslyAtURL:tmpURL completion:nil];
        }
    };
}

- (instancetype)initWithSourceURL:(NSURL *)aURL destinationURL:(NSURL *)destinationURL
                       completion:(SGCompletionBlock)completionBlock {
    self = [super init];
    
    if (self) {
        _unzipSourceURL      = aURL;
        _unzipDestinationURL = destinationURL;
        
        [self setSGCompletionBlock:completionBlock];
    }
    
    return self;
}

- (void)start {
    [self addSGExecutionBlock:[[self class] unzipOperationBlock]];
    [super start];
}

- (float)progress {
    float outValue = [super progress];
    
    if (_unzipProgress < 1.0f) {
        outValue += _unzipProgress / self.totalExpectedBlockCount;
    }
    
    return outValue;
}

@end
