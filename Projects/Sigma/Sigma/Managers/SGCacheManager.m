//
//  SGURLCacheManager.m
//  SGApp
//
//  Created by sangwoo.im on 5/8/13.
//
//

#import "SGCacheManager_Private.h"

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
static NSUInteger const SGMaximumResourceLoadings = 8;
#else
static NSUInteger const SGMaximumResourceLoadings = 32;
#endif

@implementation SGCacheManager

+ (SGCacheManager *)sharedManager {
    static SGCacheManager * manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString * const queueName = SGNSString(@"com.sigma.%@", NSStringFromClass(self));
        
        dispatch_queue_t const queue = [SGManager queueWithName:queueName
                                                       priority:DISPATCH_QUEUE_PRIORITY_BACKGROUND
                                                     concurrent:YES];
        
        manager = [[self alloc] initWithQueue:queue];
    });
    
    return manager;
}

- (id)initWithQueue:(dispatch_queue_t)queue {
    self = [super initWithQueue:queue];
    if (self) {
        _loadingQueue = [[SGOperationQueue alloc] initWithMaximumNumberOfOperations:SGMaximumResourceLoadings];
        _cacheMap     = [NSMutableDictionary new];
        if ([self respondsToSelector:@selector(setUpSystemDependency)]) {
            [self performSelector:@selector(setUpSystemDependency)];
        }
    }
    return self;
}

- (void)dealloc {
    if ([self respondsToSelector:@selector(tearDownSystemDependency)]) {
        [self performSelector:@selector(tearDownSystemDependency)];
    }
}

- (void)unloadCache {
    [self performCriticalBlock:^{
        [[_cacheMap allValues] makeObjectsPerformSelector:@selector(unloadCache)];
    }];
}

- (void)setObject:(id)anObject forKey:(id<NSCopying>)aKey {
    [self performCriticalBlock:^{
        _cacheMap[aKey] = anObject;
    }];
}

- (id)objectForKey:(id<NSCopying>)aKey {
    __block id object = nil;
    
    [self performBlockSynchronously:^{
        object = _cacheMap[aKey];
    }];
    
    return object;
}

- (void)removeObjectForKey:(id<NSCopying>)aKey {
    [self performCriticalBlock:^{
        [_cacheMap removeObjectForKey:aKey];
    }];
}

- (void)setObject:(id)anObject forKeyedSubscript:(id<NSCopying>)aKey {
    [self setObject:anObject forKey:aKey];
}

- (id)objectForKeyedSubscript:(id<NSCopying>)aKey {
    return [self objectForKey:aKey];
}

@end
