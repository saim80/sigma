//
//  SGManager.h
//  SGApp
//
//  Created by Sangwoo Im on 3/10/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

@class SGObject;

//! Manager schedules and synchronzes incoming tasks with thread safety.
//! All submitted tasks are performed on GCD queues. When a task is complete, the task completion is notified via completionQueue.
//! When completionQueue is nil, the completion is notified on the currently running queue.
//!
//! # Subclassing Note
//!
//! Subclass must incorporate completionQueue when notifying task completions.
//!
@interface SGManager : NSObject
//! Determines if there is an active queue running.
@property (nonatomic, assign, getter = isActive) BOOL active;
@property (nonatomic, strong) dispatch_queue_t completionQueue;

//! Returns GCD main queue.
+ (dispatch_queue_t)mainQueue;
//! Returns GCD global queue with default priority.
+ (dispatch_queue_t)globalQueue;
//! Returns GCD global queue with a given priority.
+ (dispatch_queue_t)globalQueueWithPriority:(dispatch_queue_priority_t)priority;
//! Returns a new GCD queue with given name, priority, and concurrency option.
+ (dispatch_queue_t)queueWithName:(NSString *)name
                         priority:(dispatch_queue_priority_t)priority
                       concurrent:(BOOL)concurrent;
//! Performes a given block on main thread by using dispatch main queue.
+ (void)performBlockOnMainThread:(dispatch_block_t)block;

//! Returns a manager object with a given queue.
- (id)initWithQueue:(dispatch_queue_t)queue;

//! Determines if there is an active queue running.
- (BOOL)isActive;
//! Suspends all submitted blocks except the one currently runnning.
- (void)suspend;
//! Resumes block excutions.
- (void)resume;
//! Determines if the block in the current stack frame is being executed by this manager's queue or not.
- (BOOL)isRunning;
//! Determines if the queue is in critical section.
- (BOOL)isInCriticalSection;

//! Performs critical block operation asynchronously. The given block will be executed mutually exclusive critical section.
- (void)performCriticalBlock:(dispatch_block_t)block;
//! Performs block operation asynchronously. 
- (void)performBlock:(dispatch_block_t)block;
//! Performs block operation synchronously.
- (void)performBlockSynchronously:(dispatch_block_t)block;

//! Performs a given block with thread-safety after checking the availability of a given property.
//! Use this method only for write access to a given object.
- (void)setValueSafelyForObject:(SGObject *)object withBlock:(dispatch_block_t)block;
//! Performs a given block with thread-safety after checking the availability of a given property.
//! Use this method only for read access to a given object.
- (void)getValueSafelyForObject:(SGObject *)object withBlock:(dispatch_block_t)block;

@end
