//
//  SGSessionManager.m
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGSessionManager_Private.h"

static const NSUInteger SGSessionManagerOperationMaxCount = 4;

@implementation SGSessionManager
@dynamic networkReachable;
@dynamic HTTPSession;

- (instancetype)initWithQueue:(dispatch_queue_t)queue baseURL:(NSURL *)baseURL {
    self = [super initWithQueue:queue];
    
    if (self) {
        _HTTPManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        _reachabilityManager = [AFNetworkReachabilityManager managerForDomain:[baseURL host]];
        _queue = [[SGOperationQueue alloc] initWithMaximumNumberOfOperations:SGSessionManagerOperationMaxCount];
        
        _currentAccount = [self _accountForPreviousSession];
        
        __weak SGSessionManager * const weakSelf = self;
        
        [_reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            [[NSNotificationCenter defaultCenter] postNotificationName:SGSessionManagerNetworkAvailabilityDidChange
                                                                object:weakSelf];
        }];
        
        if ([self respondsToSelector:@selector(setUpSystemDependency)]) {
            [self performSelector:@selector(setUpSystemDependency)];
        }
    }
    
    return self;
}

- (AFHTTPSessionManager *)HTTPSession {
    return _HTTPManager;
}

- (void)dealloc {
    if ([self respondsToSelector:@selector(tearDownSystemDependency)]) {
        [self performSelector:@selector(tearDownSystemDependency)];
    }
}

- (SGAccount *)_accountForPreviousSession {
    NSUserDefaults * const defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * const userInfo = [defaults dictionaryForKey:SGSessionManagerCurrentAccountKey];
    SGAccount * const account = [SGAccount new];
    
    account.manager  = self;
    account.userInfo = userInfo;
    
    [account reload];
    
    return account;
}


- (void)startNewSession:(NSString *)userID
               password:(NSString *)password
                success:(SGNetworkSuccessBlock)success
                failure:(SGNetworkFailureBlock)failure {
    
    [_authTask cancel];
    
    if (userID && password) {
        [_currentAccount reset];
        
        _currentAccount.userInfo = @{ SGAccountUserKey : userID };
        
        [_currentAccount authenticateWithPassword:password success:success failure:failure];
    }
}

- (void)_validateSession {
    SGSessionManager * __weak weakSelf = self;
    
    [_validateTask cancel];
    
    if (_reachabilityManager.reachable) {
        _validateTask =
        [self.currentAccount validateWithSuccess:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (weakSelf.validationBlock) {
                weakSelf.validationBlock();
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable responseObject) {
            [weakSelf.currentAccount reset];
        }];
    }
}

- (BOOL)networkReachable {
    return _reachabilityManager.reachable;
}

@end
