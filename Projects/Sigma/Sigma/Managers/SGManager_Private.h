//
//  SGManager_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 3/30/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGManager.h"
#import "SGObject.h"

@interface SGManager () {
@private
    BOOL _active;
    BOOL _isInCriticalSection;
@protected
    dispatch_queue_t _queue;
}

- (void)_notifyCompletionIfNecessary:(dispatch_block_t)completion;

@end