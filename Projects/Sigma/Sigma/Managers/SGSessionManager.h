//
//  SGSessionManager.h
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGManager.h"
#import "SGFoundation_Types.h"

@class AFHTTPSessionManager, AFNetworkReachabilityManager;
@class SGAccount, SGOperationQueue;

OBJC_EXTERN NSString * const _Nonnull SGSessionManagerNetworkAvailabilityDidChange;

OBJC_EXTERN NSString * const _Nonnull SGSessionManagerCurrentAccountKey;

/// Network session management. Maintains user sessions. This also can persist user session until expired.
@interface SGSessionManager : SGManager
/// If any operation scheduling is required for session management. Then, use this.
@property (nonatomic, readonly, nonnull) SGOperationQueue *queue;
/// Account associated with the current user.
@property (nonatomic, strong, nullable) SGAccount *currentAccount;
/// Invoked when session is re-validated. This often occurs when the app becomes active.
@property (nonatomic, copy, nonnull) dispatch_block_t validationBlock;
/// Determines if the network is reachable.
@property (nonatomic, assign) BOOL networkReachable;
/// HTTP Session manager.
@property (nonatomic, readonly, nonnull) AFHTTPSessionManager *HTTPSession;

- (nonnull instancetype)initWithQueue:(nullable dispatch_queue_t)queue baseURL:(nonnull NSURL *)baseURL;

- (void)startNewSession:(nonnull NSString *)userID
               password:(nonnull NSString *)password
                success:(nullable SGNetworkSuccessBlock)success
                failure:(nullable SGNetworkFailureBlock)failure;

@end
