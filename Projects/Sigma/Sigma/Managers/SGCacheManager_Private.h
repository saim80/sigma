//
//  SGURLCacheManager_Private.h
//  SGApp
//
//  Created by sangwoo.im on 5/8/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGCacheManager.h"
#import "SGOperationQueue.h"
#import "SGFoundation.h"

@interface SGCacheManager ()
@property (nonatomic, strong) SGOperationQueue    *loadingQueue;
@property (nonatomic, strong) NSMutableDictionary *cacheMap;
@property (nonatomic, strong) id                  observerToken;

@end
