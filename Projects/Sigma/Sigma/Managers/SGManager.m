//
//  SGManager.m
//  SGApp
//
//  Created by Sangwoo Im on 3/10/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGManager_Private.h"

#define SGManagerIsRunningKey (__bridge void *)(self)


@implementation SGManager

+ (dispatch_queue_t)mainQueue {
    return dispatch_get_main_queue();
}

+ (dispatch_queue_t)globalQueue {
    return [self globalQueueWithPriority:DISPATCH_QUEUE_PRIORITY_DEFAULT];
}

+ (dispatch_queue_t)globalQueueWithPriority:(dispatch_queue_priority_t)priority {
    return dispatch_get_global_queue(priority, (unsigned long)NULL);
}

+ (void)performBlockOnMainThread:(dispatch_block_t)block {
    if ([[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

+ (dispatch_queue_t)queueWithName:(NSString *)name priority:(dispatch_queue_priority_t)priority concurrent:(BOOL)concurrent {
    const char *                queueName    = [name cStringUsingEncoding:NSUTF8StringEncoding];
    dispatch_queue_attr_t const isConcurrent = (concurrent ? DISPATCH_QUEUE_CONCURRENT : DISPATCH_QUEUE_SERIAL);
    dispatch_queue_t      const queue        = dispatch_queue_create(queueName, isConcurrent);
    
    return queue;
}

- (id)initWithQueue:(dispatch_queue_t)aQueue {
    self = [super init];
    
    if (self) {
        dispatch_queue_t managerQueue = nil;
        
        if (!aQueue) {
            managerQueue = [SGManager mainQueue];
        } else {
            managerQueue = aQueue;
        }
        
        _queue = managerQueue;
        
        if (_queue) {
            dispatch_queue_set_specific(_queue, SGManagerIsRunningKey, (__bridge void *)(self), NULL);
            _active = YES;
        }
    }
    
    return self;
}

- (void)dealloc {
    if (_queue) {
        dispatch_queue_set_specific(_queue, SGManagerIsRunningKey, NULL, NULL);
    }
    
    _active = NO;
}

- (void)_notifyCompletionIfNecessary:(dispatch_block_t)completion {
    if (completion) {
        if (_completionQueue) {
            dispatch_async(_completionQueue, completion);
        } else {
            completion();
        }
    }
}

#pragma mark - Properties

- (dispatch_queue_t)queue {
    return _queue;
}

- (void)setActive:(BOOL)active {
    if (active) {
        [self resume];
    } else {
        [self suspend];
    }
}

- (BOOL)isActive {
    __block BOOL state = NO;
    
    [self performBlockSynchronously:^{
        state = _active;
    }];
    
    return state;
}

#pragma mark - Public

- (void)suspend {
    [self performCriticalBlock:^{
        if (_active) {
            _active = NO;
            if (_queue) {
                dispatch_suspend(_queue);
            }
        }
    }];
}
- (void)resume {
    [self performCriticalBlock:^{
        if (!_active) {
            _active = YES;
            if (_queue) {
                dispatch_resume(_queue);
            }
        }
    }];
}

- (BOOL)isRunning {
    return (dispatch_get_specific(SGManagerIsRunningKey) == (__bridge void *)(self));
}

- (BOOL)isInCriticalSection {
    __block BOOL state = NO;
    
    [self performBlockSynchronously:^{
        state = _isInCriticalSection;
    }];
    
    return state;
}

- (void)performCriticalBlock:(dispatch_block_t)block {
    if ([self isRunning] && _isInCriticalSection) {
        block();
    } else if (_queue) {
        dispatch_barrier_async(_queue, ^{
            _isInCriticalSection = YES;
            block();
            _isInCriticalSection = NO;
        });
    }
}

- (void)performBlock:(dispatch_block_t)block {
    dispatch_async(_queue, block);
}

- (void)performBlockSynchronously:(dispatch_block_t)block {
    if ([self isRunning]) {
        block();
    } else {
        dispatch_sync(_queue, block);
    }
}

- (void)_safelySetValueWithQueueConfinementKey:(void *)key withBlock:(dispatch_block_t)block {
    if (block) {
        [self performCriticalBlock:^{
            if (key) {
                block();
            }
        }];
    }
}

- (void)_safelyGetValueWithQueueConfinementKey:(void *)key withBlock:(dispatch_block_t)block {
    if (block) {
        [self performBlockSynchronously:^{
            if (key) {
                block();
            }
        }];
    }
}

- (void)setValueSafelyForObject:(SGObject *)object withBlock:(dispatch_block_t)block {
    if ([object manager] == self) {
        [self _safelySetValueWithQueueConfinementKey:[object keyForQueueConfinement] withBlock:block];
    }
}

- (void)getValueSafelyForObject:(SGObject *)object withBlock:(dispatch_block_t)block {
    if ([object manager] == self) {
        [self _safelyGetValueWithQueueConfinementKey:[object keyForQueueConfinement] withBlock:block];
    }
}

@end
