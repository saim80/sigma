//
//  SGSessionManager_Private.h
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGSessionManager.h"
#import <AFNetworking/AFNetworking.h>
#import "SGAccount.h"
#import "SGOperationQueue.h"
#import "SGBlockOperation.h"
#import <SSKeychain.h>

NSString * const _Nonnull SGSessionManagerNetworkAvailabilityDidChange = @"com.sigma.session-manager.network-availability-change";
NSString * const _Nonnull SGSessionManagerCurrentAccountKey = @"com.sigma.session-manager.current-account";

@interface SGSessionManager ()
/// HTTP manager.
@property (nonatomic, readonly, nonnull) AFHTTPSessionManager *HTTPManager;
/// Reachability manager.
@property (nonatomic, readonly, nonnull) AFNetworkReachabilityManager *reachabilityManager;

@property (nonatomic, weak, nullable) NSURLSessionDataTask *authTask;
@property (nonatomic, weak, nullable) NSURLSessionDataTask *validateTask;

- (nonnull SGAccount *)_accountForPreviousSession;
- (void)_validateSession;

@end