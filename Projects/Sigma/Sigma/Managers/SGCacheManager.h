//
//  SGURLCacheManager.h
//  SGApp
//
//  Created by sangwoo.im on 5/8/13.
//
//

#import "SGManager.h"

@class SGURLCache;

//! Maintains multiple SGURLCache objects in a dictionary. Manager provides shared dispatch queue and SGOperationQueue
//! for its SGURLCache objects to synchronize.
//!
//! ## Creating a SGURLCache object
//!
//! It is okay to use designated initialization method, [SGURLCache initWithLocalDirectoryURL:lifespan:]. However,
//! a factory method is provided at manager level for convenience. [SGURLCacheManager cacheWithKey:lifespan:].
//!
//! ## Unloading Cache
//!
//! [SGURLCacheManager unloadCache] unloads all in-memory cache. This is called automatically on memory warning for iOS.
//! File system cache won't be deleted by this method. See SGURLCache for file system cache removal.
//!
@interface SGCacheManager : SGManager

+ (instancetype)sharedManager;
- (void)unloadCache;

#pragma mark - Cache Accsessors

- (void)setObject:(id)anObject forKey:(id<NSCopying>)aKey;
- (id)objectForKey:(id<NSCopying>)aKey;
- (void)removeObjectForKey:(id<NSCopying>)aKey;

#pragma mark - Boxed Literal Subscript Support

- (void)setObject:(id)anObject forKeyedSubscript:(id<NSCopying>)aKey;
- (id)objectForKeyedSubscript:(id<NSCopying>)aKey;

@end
