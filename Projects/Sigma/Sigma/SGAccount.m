//
//  SGAccount.m
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGAccount_Private.h"

NSString * const SGAccountServiceName = @"com.sigma.account-service";
NSString * const SGAccountAuthHeader  = @"Authorization";

@implementation SGAccount

- (nullable NSURLSessionDataTask *)authenticateWithPassword:(nullable NSString *)password success:(nullable SGNetworkSuccessBlock)successBlock failure:(nullable SGNetworkFailureBlock)errorBlock {
    [SGFoundation raiseException:SGFoundationAbstractClassInstantiationException file:__FILE__ line:__LINE__];
    return nil;
}
- (nullable NSURLSessionDataTask *)validateWithSuccess:(nullable SGNetworkSuccessBlock)completeionBlock failure:(nullable SGNetworkFailureBlock)errorBlock {
    [SGFoundation raiseException:SGFoundationAbstractClassInstantiationException file:__FILE__ line:__LINE__];
    return nil;
}

- (void)setUserInfo:(NSDictionary *)userInfo {
    [self setValueSafelyWithBlock:^(SGAccount * _Nullable weakSelf) {
        if (weakSelf) {
            weakSelf->_userInfo = userInfo;
        }
    }];
}

- (void)setAccessToken:(NSString *)accessToken {
    [self setValueSafelyWithBlock:^(SGAccount *weakSelf) {
        SGAccount * const strongSelf = weakSelf;
        if (![strongSelf->_accessToken isEqualToString:accessToken]) {
            SGSessionManager        * const manager    = (id)strongSelf.manager;
            AFHTTPRequestSerializer * const serializer = (id)manager.HTTPSession.requestSerializer;
            
            strongSelf->_accessToken = accessToken;
            
            [serializer setValue:accessToken forHTTPHeaderField:SGAccountAuthHeader];
        }
    }];
}

- (void)reload {
    [self setValueSafelyWithBlock:^(SGAccount *weakSelf) {
        if (weakSelf.userInfo) {
            NSString * const accountName = weakSelf.userInfo[SGAccountUserKey];
            
            if (accountName) {
                NSString * const accessToken = [SSKeychain passwordForService:SGAccountServiceName account:accountName];
                
                weakSelf.accessToken = accessToken;
            }
        }
    }];
}
- (void)reset {
    [self setValueSafelyWithBlock:^(SGAccount *weakSelf) {
        if (weakSelf.userInfo) {
            NSUserDefaults * const defaults = [NSUserDefaults standardUserDefaults];
            NSString * const accountName = weakSelf.userInfo[SGAccountUserKey];
            
            if (accountName) {
                [SSKeychain deletePasswordForService:SGAccountServiceName account:accountName];
            }
            [defaults removeObjectForKey:SGSessionManagerCurrentAccountKey];
        }
        
        weakSelf.userInfo = nil;
        weakSelf.accessToken = nil;
        weakSelf.userInfo = nil;
    }];
}
- (void)save {
    [self getValueSafelyWithBlock:^(SGAccount *weakSelf) {
        if (weakSelf.userInfo) {
            NSString * const accountName = weakSelf.userInfo[SGAccountUserKey];
            
            if (accountName && weakSelf.accessToken) {
                [SSKeychain setPassword:self.accessToken forService:SGAccountServiceName account:accountName];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:weakSelf.userInfo forKey:SGSessionManagerCurrentAccountKey];
        }
    }];
}

@end
