//
//  SGConfig_Private.h
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGConfig.h"

NSString * const SGConfigDidChangeValue = @"com.sigma.config.did-change-value";
NSString * const SGConfigKey = @"com.sigma.config.key";
NSString * const SGConfigValue = @"com.sigma.config.value";

@interface SGConfig ()
@property (nonatomic, strong) NSDictionary *constantData;
@property (nonatomic, strong) NSDictionary *dynamicData;

@end