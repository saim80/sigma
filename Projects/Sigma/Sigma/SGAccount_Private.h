//
//  SGAccount_Private.h
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGAccount.h"
#import "SGSessionManager.h"
#import "AFNetworking.h"
#import "SGFoundation.h"
#import <SSKeychain/SSKeychain.h>

NSString * const SGAccountUserKey = @"com.sigma.account.user";

@interface SGAccount ()

@end