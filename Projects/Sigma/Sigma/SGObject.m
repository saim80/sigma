//
//  SGObject.m
//  SGApp
//
//  Created by sangwoo.im on 3/19/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGObject_Private.h"

@implementation SGObject

- (void)setEvent:(NSString *)eventName handler:(SGEventBlock)handler {
    [self setValueSafelyWithBlock:^(SGObject *weakSelf) {
        if ([eventName length] > 0) {
            if (handler) {
                if (![weakSelf eventHandlers]) {
                    weakSelf.eventHandlers = [NSMutableDictionary new];
                }
                
                weakSelf.eventHandlers[eventName] = [handler copy];
            } else {
                [weakSelf.eventHandlers removeObjectForKey:eventName];
                if ([weakSelf.eventHandlers count] == 0) {
                    weakSelf.eventHandlers = nil;
                }
            }
        }
    }];
}
- (SGEventBlock)handlerForEvent:(NSString *)eventName {
    __block SGEventBlock block = nil;
    
    [self getValueSafelyWithBlock:^(SGObject *weakSelf) {
        if ([eventName length] > 0) {
            block = weakSelf.eventHandlers[eventName];
        }
    }];
    
    return block;
}
- (void)performEvent:(NSString *)event sender:(id)sender {
    [self performEvent:event sender:sender info:nil];
}
- (void)performEvent:(NSString *)event sender:(id)sender info:(NSDictionary *)eventInfo {
    [self getValueSafelyWithBlock:^(id weakSelf) {
        SGEventBlock const block = [self handlerForEvent:event];
        
        if (block) {
            block(sender, eventInfo);
        }
    }];
}

- (void *)keyForQueueConfinement {
    return (__bridge void *)self;
}

- (void)setValueSafelyWithBlock:(SGReflectiveBlock)block {
    __weak id weakSelf = self;
    if (_manager) {
        [_manager setValueSafelyForObject:self withBlock:^{
            block(weakSelf);
        }];
    } else {
        block(weakSelf);
    }
}
- (void)getValueSafelyWithBlock:(SGReflectiveBlock)block {
    __weak id weakSelf = self;
    if (_manager) {
        [_manager getValueSafelyForObject:self withBlock:^{
            block(weakSelf);
        }];
    } else {
        block(weakSelf);
    }
}

@end
