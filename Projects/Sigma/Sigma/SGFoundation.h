//
//  SGFoundation.h
//  SG App
//
//  Created by Sangwoo Im on 3/10/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#define SGLogLevelSilent  NSUIntegerMax
#define SGLogLevelFatal   5
#define SGLogLevelWarn    4
#define SGLogLevelError   3
#define SGLogLevelInfo    2
#define SGLogLevelDebug   1
#define SGLogLevelDefault 0

#ifdef DEBUG
#define SGLog(__format__, ...)   \
                     [SGFoundation logWithFile:__FILE__ line:__LINE__ logLevel:SGLogLevelDefault format:__format__, ##__VA_ARGS__]
#define SGDebug(__format__, ...) \
                     [SGFoundation logWithFile:__FILE__ line:__LINE__ logLevel:SGLogLevelDebug format:__format__, ##__VA_ARGS__]
#define SGInfo(__format__, ...)   \
                     [SGFoundation logWithFile:__FILE__ line:__LINE__ logLevel:SGLogLevelInfo format:__format__, ##__VA_ARGS__]
#define SGError(__format__, ...)  \
                     [SGFoundation logWithFile:__FILE__ line:__LINE__ logLevel:SGLogLevelError format:__format__, ##__VA_ARGS__]
#define SGWarn(__format__, ...)   \
                     [SGFoundation logWithFile:__FILE__ line:__LINE__ logLevel:SGLogLevelWarn format:__format__, ##__VA_ARGS__]
#define SGFatal(__format__, ...)  \
                     [SGFoundation logWithFile:__FILE__ line:__LINE__ logLevel:SGLogLevelFatal format:__format__, ##__VA_ARGS__]
#else
#define SGLog(__format__, ...)
#define SGDebug(__format__, ...)
#define SGInfo(__format__, ...)
#define SGError(__fotmat__, ...)
#define SGWarn(__format__, ...)
#define SGFatal(__format__, ...)
#endif

#define SGNSString(__format__, ...) [[NSString alloc] initWithFormat:__format__, ##__VA_ARGS__]

//! Project version number for Sigma.
FOUNDATION_EXPORT double SigmaVersionNumber;

//! Project version string for Sigma.
FOUNDATION_EXPORT const unsigned char SigmaVersionString[];

typedef NS_ENUM(NSInteger, SGFoundationException) {
    SGFoundationAbstractClassInstantiationException = -100,
    SGFoundationDesignatedInitializerException      = -101
};

OBJC_EXTERN NSString * const SGFoundationExceptionLine;
OBJC_EXTERN NSString * const SGFoundationExceptionFile;

@interface SGFoundation : NSObject

+ (void)setLogLevel:(NSUInteger)logLevel;
+ (NSURL *)documentsURL;
+ (NSURL *)applicationSupportURL;
+ (NSURL *)cachesURL;
+ (NSURL *)temporaryURL;
+ (NSURL *)applicationURL;
+ (NSURL *)replacementURL;
+ (void)raiseException:(SGFoundationException)exception file:(char *)file line:(int)line;
+ (void)raiseException:(SGFoundationException)exception info:(NSDictionary *)info;
+ (void)raiseExceptionWithReason:(NSString *)reason file:(char *)file line:(int)line;
+ (void)logWithFile:(const char *)file line:(unsigned long)line logLevel:(NSUInteger)logLevel format:(NSString *)format, ...;

@end
