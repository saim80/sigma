//
//  SGAccount.h
//  Sigma
//
//  Created by Sangwoo Im on 1/5/16.
//  Copyright © 2016 Sangwoo Im. All rights reserved.
//

#import "SGObject.h"

OBJC_EXTERN NSString * const _Nonnull SGAccountUserKey;

/// Represents application user account.
/// User id and access token are saved to encrypted keychain.
@interface SGAccount : SGObject
/// Password. This is a volatile information. We keep this value until authentication call. Otherwise, this is always nil.
@property (nonatomic, readonly, nullable) NSString * accessToken;
/// Current user info. Use this dictionary to save user information in insecure manner. This value becomes nil when
/// session invalidates.
@property (nonatomic, strong, nullable) NSDictionary *userInfo;

/// Returns a session task with authentication.
///
/// This is a placeholder. Subclass needs to implement this method.
- (nullable NSURLSessionDataTask *)authenticateWithPassword:(nullable NSString *)password
                                                    success:(nullable SGNetworkSuccessBlock)success
                                                    failure:(nullable SGNetworkFailureBlock)failure;
/// Returns a session task with account validation.
///
/// This is a placeholder. Subclass needs to implement this method.
- (nullable NSURLSessionDataTask *)validateWithSuccess:(nullable SGNetworkSuccessBlock)success
                                               failure:(nullable SGNetworkFailureBlock)failure;

/// Loads existing account data from persistent store.
- (void)reload;
/// Resets account info both in-memory and persistent store.
- (void)reset;
/// Saves the current account info into presistent store.
- (void)save;

@end
