//
//  SGFoundation_Types.h
//  SGApp
//
//  Created by Sangwoo Im on 3/22/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

@import Foundation;

@class SGBlockOperation, SGDataModelManager;

typedef void (^SGExecutionBlock) (SGBlockOperation * _Nonnull operation);
typedef void (^SGCompletionBlock) (SGBlockOperation * _Nonnull operation);

typedef void (^SGNetworkSuccessBlock) ( NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject);
typedef void (^SGNetworkFailureBlock) ( NSURLSessionDataTask * _Nonnull task, NSError * _Nullable responseObject);

typedef void (^SGEventBlock) (id _Nullable sender, NSDictionary * _Nullable eventInfo);
typedef void (^SGReflectiveBlock) (id _Nullable weakSelf);

typedef void (^SGDataLoadCompletionBlock) (id _Nullable data, NSError * _Nullable error, NSDictionary * _Nullable info);
typedef void (^SGDataModelCompletionBlock) (NSError * _Nullable error, NSDictionary * _Nullable info);
typedef void (^SGDataModelSetCompletionBlock) (NSArray * _Nullable resultArray, NSError * _Nullable error, NSDictionary * _Nullable info);
typedef void (^SGDataModelSQLBlock) (SGDataModelManager * _Nullable weakManager, NSError *__autoreleasing _Nullable * _Nullable error);
