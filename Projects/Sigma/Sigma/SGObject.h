//
//  SGObject.h
//  SGApp
//
//  Created by sangwoo.im on 3/19/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGFoundation_Types.h"

@class SGManager;

/// A base object class to support thread safety.
@interface SGObject : NSObject
//! back refernece to model data.
@property (nonatomic, weak) id userData;
//! weak reference to manager
@property (nonatomic, weak) SGManager *manager;

- (void)setEvent:(NSString *)eventName handler:(SGEventBlock)handler;
- (SGEventBlock)handlerForEvent:(NSString *)eventName;
- (void)performEvent:(NSString *)event sender:(id)sender;
- (void)performEvent:(NSString *)event sender:(id)sender info:(NSDictionary *)eventInfo;

//! Returns 'self' by default. Subclass can return NULL to avoid queue confinement.
- (void *)keyForQueueConfinement;

- (void)setValueSafelyWithBlock:(SGReflectiveBlock)block;
- (void)getValueSafelyWithBlock:(SGReflectiveBlock)block;

@end
