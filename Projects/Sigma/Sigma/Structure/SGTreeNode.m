//
//  SGTreeNode.m
//  SGApp
//
//  Created by Sangwoo Im on 4/11/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGTreeNode_Private.h"

@implementation SGTreeNode

- (id)init {
    self = [super init];
    
    if (self) {
        _mutableChildren = [NSMutableOrderedSet new];
    }
    
    return self;
}

- (id)initWithParent:(SGTreeNode *)parent children:(NSArray *)children {
    self = [self init];
    
    if (self) {
        _parent = parent;
        [[_parent mutableChildren] addObject:self];
        [_mutableChildren addObjectsFromArray:children];
    }
    
    return self;
}

- (NSArray *)children {
    return [_mutableChildren array];
}

- (id)copyWithZone:(NSZone *)zone {
    SGTreeNode *copiedNode = [[SGTreeNode allocWithZone:zone] initWithParent:_parent
                                                                    children:[_mutableChildren array]];
    copiedNode.tag = self.tag;
    
    return copiedNode;
}

- (id)mutableCopyWithZone:(NSZone *)zone {
    SGMutableTreeNode * copiedNode = [[SGMutableTreeNode allocWithZone:zone] initWithParent:_parent
                                                                                   children:[_mutableChildren array]];
    
    copiedNode.tag = self.tag;
    
    return copiedNode;
}

@end

//--------------------------------------------------------------------------------------------------------------------------------

@implementation SGMutableTreeNode

- (id)init {
    self = [super init];
    
    if (self) {
        _mutableChildren = [NSMutableOrderedSet new];
        _localIndex      = NSNotFound;
    }
    
    return self;
}

- (id)initWithParent:(SGTreeNode *)parent children:(NSArray *)children {
    NSAssert(parent == nil || [parent isKindOfClass:[SGMutableTreeNode class]],
             @"invalid parent. Parent must be a subclass of SGMutableTreeNode.");
    self = [super initWithParent:parent children:children];
    
    if (self) {
        _localIndex = [[_parent mutableChildren] indexOfObject:self];
    }
    
    return self;
}

- (void)willMoveToParent:(SGMutableTreeNode *)parent {
    
}
- (void)didMoveToParent {
    
}

- (void)addChild:(SGMutableTreeNode *)child {
    if (![_mutableChildren containsObject:child]) {
        [child removeFromParent];
        [child willMoveToParent:self];
        [child _setParent:self];
        [_mutableChildren addObject:child];
        NSAssert([_mutableChildren indexOfObject:child] == child.localIndex, @"Invalid local index.");
        [child didMoveToParent];
    }
}
- (void)removeFromParent {
    if (_parent) {
        [self willMoveToParent:nil];
        NSAssert([_parent mutableChildren][_localIndex] == self, @"Invalid local index.");
        [[_parent mutableChildren] removeObjectAtIndex:_localIndex];
        [self _setParent:nil];
        [self didMoveToParent];
    }
}

- (void)_setParent:(SGMutableTreeNode *)parent {
    _parent = parent;
}

- (void)_setLocalIndex:(NSUInteger)localIndex {
    _localIndex = localIndex;
}

- (void)insertChild:(SGMutableTreeNode *)child atIndex:(NSUInteger)index {
    if (![_mutableChildren containsObject:child]) {
        [child removeFromParent];
        [child willMoveToParent:self];
        [child _setParent:self];
        [_mutableChildren insertObject:child atIndex:index];
        NSAssert([_mutableChildren indexOfObject:child] == index, @"Invalid local index.");
        [child didMoveToParent];
    }
}

@end
