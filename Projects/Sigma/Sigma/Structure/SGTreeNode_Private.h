//
//  SGTreeNode_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 4/11/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

#import "SGTreeNode.h"

@interface SGTreeNode () {
@protected
    NSMutableOrderedSet *_mutableChildren;
    
    __weak SGTreeNode *_parent;
}

@property (nonatomic, strong, readonly) NSMutableOrderedSet *mutableChildren;

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGMutableTreeNode () {
    NSUInteger _localIndex;
}

@property (nonatomic, assign, readonly) NSUInteger localIndex;

- (void)_setParent:(SGTreeNode *)parent;

@end
