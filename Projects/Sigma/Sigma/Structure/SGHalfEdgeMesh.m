//
//  SGHalfEdgeMesh2D.m
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGHalfEdgeMesh_Private.h"

@implementation SGHalfEdgeMesh

- (instancetype)init {
    self = [super init];
    
    if (self) {
        _allFaces            = [NSMutableArray new];
        _cachedBoundaryFaces = [NSMutableSet new];
    }
    
    return self;
}

- (instancetype)initWithMesh:(SGHalfEdgeMesh *)mesh {
    self = [self init];
    
    if (self) {
        [[mesh allFaces] enumerateObjectsUsingBlock:^(SGHalfEdgeFace *face, NSUInteger idx, BOOL *stop) {
            face.mesh = self;
        }];
        
        _allFaces            = [[mesh allFaces] mutableCopy];
        _cachedBoundaryFaces = [[mesh cachedBoundaryFaces] mutableCopy];
    }
    
    return self;
}

+ (NSArray *)_boundaryEdgeLoopWithHead:(SGHalfEdgeFace *)head visitedSet:(NSMutableSet *)visited {
    NSMutableArray *edges = [NSMutableArray new];
    
    [[head boundaryEdges] enumerateObjectsUsingBlock:^(SGHalfEdge *edge, NSUInteger idx, BOOL *stop) {
        if (![visited containsObject:edge]) {
            SGHalfEdge *nextEdge = edge;
            [edges addObject:edge];
            [visited addObject:edge];
            
            while ((nextEdge = [[self class] _nextBoundaryHalfEdge:nextEdge]) && nextEdge != edge) {
                [edges addObject:nextEdge];
                [visited addObject:nextEdge];
            }
        }
    }];
    
    return [edges copy];
}

- (NSArray *)boundaryEdges {
    if (!_cachedBoundaryEdges) {
        _cachedBoundaryEdges = [NSMutableArray new];
        
        NSMutableSet * const visitedSet = [NSMutableSet new];
        
        [_cachedBoundaryFaces enumerateObjectsUsingBlock:^(SGHalfEdgeFace *face, BOOL *stop) {
            NSArray * const loop = [[self class] _boundaryEdgeLoopWithHead:face visitedSet:visitedSet];
            
            [_cachedBoundaryEdges addObjectsFromArray:loop];
        }];
    }
    
    return [_cachedBoundaryEdges copy];
}

- (NSArray *)boundaryFaces {
    return [_cachedBoundaryFaces allObjects];
}

- (NSArray *)faces {
    return [_allFaces copy];
}

#pragma mark - Analysis

+ (SGHalfEdge *)_nextBoundaryHalfEdge:(SGHalfEdge *)edge {
    SGHalfEdge *outEdge = nil;
    
    SGHalfEdge *next         = [edge next];
    SGHalfEdge *nextOpposite = [next opposite];
    
    while (nextOpposite && next != edge) {
        next         = [nextOpposite next];
        nextOpposite = [next opposite];
    }
    
    if (next != edge) {
        outEdge = next;
    }
    
    return outEdge;
}

- (SGHalfEdge *)_edgeOppositeToEdge:(SGHalfEdge *)edgeA {
    __block SGHalfEdge *outEdge = nil;
    
    NSArray * const edges = [self boundaryEdges];
    
    [edges enumerateObjectsUsingBlock:^(SGHalfEdge *edgeB, NSUInteger idx, BOOL *stop) {
        if ([[SGHalfEdgeFace edgeRelation] isEdge:edgeA oppositeOfEdge:edgeB]) {
            outEdge = edgeB;
            *stop = YES;
        }
        
        if (outEdge) {
            *stop = YES;
        }
    }];
    
    return outEdge;
}

- (BOOL)_rebuildBoundaryAddingFace:(SGHalfEdgeFace *)face {
    __block BOOL duplicate = NO;
    
    [[face edges] enumerateObjectsUsingBlock:^(SGHalfEdge *edge, NSUInteger idx, BOOL *stop) {
        SGHalfEdge * const existing = [self _edgeOppositeToEdge:edge];
        
        if (existing) {
            if (!existing.opposite) {
                existing.opposite = edge;
                edge.opposite     = existing;
                
                if (![edge.face isBoundaryFace]) {
                    [_cachedBoundaryFaces removeObject:face];
                }
            } else {
                *stop = duplicate = YES;
            }
        }
    }];
    
    if (!duplicate && [face isBoundaryFace]) {
        [_cachedBoundaryFaces addObject:face];
    }
    
    return !duplicate;
}

- (void)_rebuildBoundaryRemovingFace:(SGHalfEdgeFace *)face {
    [[face edges] enumerateObjectsUsingBlock:^(SGHalfEdge *edge, NSUInteger idx, BOOL *stop) {
        SGHalfEdge * const opposite = edge.opposite;
        
        if (opposite) {
            opposite.opposite = nil;
            edge.opposite     = nil;
        }
    }];
    
    [_cachedBoundaryFaces removeObject:face];
}

- (BOOL)addFace:(SGHalfEdgeFace *)face {
    BOOL added = NO;
    
    if ([self _rebuildBoundaryAddingFace:face]) {
        face.mesh = self;
        [_allFaces addObject:face];
        [self _resetCache];
        added = YES;
    }
    
    return added;
}

- (BOOL)insertFace:(SGHalfEdgeFace *)face atIndex:(NSUInteger)idx {
    BOOL added = NO;
    
    if ([self _rebuildBoundaryAddingFace:face]) {
        face.mesh = self;
        [_allFaces insertObject:face atIndex:idx];
        [self _resetCache];
        added = YES;
    }
    
    return added;
}

- (void)removeFace:(SGHalfEdgeFace *)face {
    const NSUInteger idx = [_allFaces indexOfObject:face];
    
    if (idx != NSNotFound) {
        [self removeFaceAtIndex:idx];
    }
}

- (void)removeFaceAtIndex:(NSUInteger)idx {
    SGHalfEdgeFace * const removeTarget = _allFaces[idx];
    
    [self _rebuildBoundaryRemovingFace:removeTarget];
    
    [_allFaces removeObjectAtIndex:idx];
    
    [self _resetCache];
}

- (void)_resetCache {
    _cachedBoundaryEdges = nil;
}

@end
