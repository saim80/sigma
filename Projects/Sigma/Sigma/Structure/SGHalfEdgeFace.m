//
//  SGHalfEdgeFace.m
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGHalfEdgeFace_Private.h"

@implementation SGHalfEdgeFace

+ (void)registerEdgeRelation:(Class)relation {
    SGHalfEdgeRelation = relation;
}

+ (void)unregisterEdgeRelation {
    SGHalfEdgeRelation = nil;
}

+ (Class)edgeRelation {
    return SGHalfEdgeRelation;
}

+ (NSUInteger)_numberOfEdges {
    return 0;
}

+ (NSArray *)edgeLoop {
    NSMutableArray * const edgeLoopArray = [NSMutableArray new];
    const NSRange edgeRange = NSMakeRange(0, [self _numberOfEdges]);
    
    for (NSUInteger i = 0; i < [self _numberOfEdges]; ++i) {
        [edgeLoopArray addObject:[SGHalfEdge nullEdge]];
    }
    
    for (NSUInteger i = 0; i < [self _numberOfEdges]; ++i) {
        const NSUInteger nextIdx = [[self class] _nextIndex:i inRange:edgeRange];
        
        SGHalfEdge *next = edgeLoopArray[nextIdx];
        SGHalfEdge *edge = edgeLoopArray[i];
        
        if ([next isNull]) {
            next = [SGHalfEdge new];
        }
        if ([edge isNull]) {
            edge = [SGHalfEdge new];
        }
        
        [next setPrevious:edge];
        [edge setNext:next];
        
        [edgeLoopArray replaceObjectAtIndex:i withObject:edge];
        [edgeLoopArray replaceObjectAtIndex:nextIdx withObject:next];
    }
    
    return [edgeLoopArray copy];
}

- (instancetype)init {
    self = [self initWithEdges:[[self class] edgeLoop]];
    return self;
}

- (instancetype)initWithEdges:(NSArray *)edges {
    self = [super init];
    
    if (self) {
        if ([edges count] == [[self class] _numberOfEdges]                    &&
            [[self class] _validateForwardLinkForEdgeArray:edges error:NULL]  &&
            [[self class] _validateBackwardLinkForEdgeArray:edges error:NULL] ) {
            
            [edges enumerateObjectsUsingBlock:^(SGHalfEdge *obj, NSUInteger idx, BOOL *stop) {
                obj.face = self;
            }];
            
            _edges = [edges copy];
        } else {
            self = nil;
        }
    }
    
    return self;
}

#pragma mark - Validation

+ (NSUInteger)_nextIndex:(NSUInteger)idx inRange:(NSRange)range {
    NSUInteger nextIdx = idx + 1;
    
    if (!NSLocationInRange(nextIdx, range)) {
        nextIdx = 0;
    }
    
    return nextIdx;
}

+ (NSUInteger)_previousIndex:(NSUInteger)idx inRange:(NSRange)range {
    NSUInteger previousIdx = idx - 1;
    
    if (!NSLocationInRange(previousIdx, range)) {
        previousIdx = range.location + range.length - 1;
    }
    
    return previousIdx;
}

+ (BOOL)_validateForwardLinkForEdgeArray:(NSArray *)edges error:(NSError *__autoreleasing*)outError {
    const NSRange edgeRange = NSMakeRange(0, [edges count]);
    
    __block NSError *error = nil;
    
#ifdef DEBUG
    
    [edges enumerateObjectsUsingBlock:^(SGHalfEdge *currentEdge, NSUInteger idx, BOOL *stop) {
        const NSUInteger nextIdx = [[self class] _nextIndex:idx + 1 inRange:edgeRange];
        
        SGHalfEdge * const next = edges[nextIdx];
        
        if (currentEdge.next != next && [SGHalfEdgeRelation isEdge:currentEdge nextOfEdge:next]) {
            *stop = YES;
            error = [NSError errorWithDomain:NSStringFromClass([self class])
                                        code:SGHalfEdgeFaceBrokenForwardLink
                                    userInfo:nil];
        }
    }];
    
#endif
    
    if (outError) {
        *outError = error;
    }
    
    return !error;
}

+ (BOOL)_validateBackwardLinkForEdgeArray:(NSArray *)edges error:(NSError *__autoreleasing*)outError {
    const NSRange edgeRange = NSMakeRange(0, [edges count]);
    
    __block NSError *error = nil;
    
#ifdef DEBUG
    
    [edges enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(SGHalfEdge *currentEdge, NSUInteger idx, BOOL *stop) {
        const NSUInteger previousIdx = [[self class] _previousIndex:idx - 1 inRange:edgeRange];
        
        SGHalfEdge * const previous = edges[previousIdx];
        
        if (currentEdge.previous != previous && [SGHalfEdgeRelation isEdge:currentEdge previousOfEdge:previous]) {
            *stop = YES;
            error = [NSError errorWithDomain:NSStringFromClass([self class])
                                        code:SGHalfEdgeFaceBrokenBackwardLink
                                    userInfo:nil];
        }
    }];
    
#endif
    
    if (outError) {
        *outError = error;
    }
    
    return !error;
}

- (BOOL)validate:(NSError *__autoreleasing*)outError {
    NSError *error = nil;
    
    if (![[self class] _validateForwardLinkForEdgeArray:_edges  error:&error] ||
        ![[self class] _validateBackwardLinkForEdgeArray:_edges error:&error] ) {
        if (outError) {
            *outError = error;
        }
    }
    
    return !error;
}

- (NSArray *)edges {
    return _edges;
}

- (SGHalfEdge *)edgeAtIndex:(NSUInteger)idx {
    const NSRange edgeRange = NSMakeRange(0, [_edges count]);
    
    SGHalfEdge *outEdge = nil;
    
    if (NSLocationInRange(idx, edgeRange)) {
        outEdge = _edges[idx];
    }
    
    return outEdge;
}

- (NSArray *)adjacentFaces {
    NSMutableArray * const faces = [NSMutableArray new];
    
    [_edges enumerateObjectsUsingBlock:^(SGHalfEdge * const edge, NSUInteger idx, BOOL *stop) {
        SGHalfEdge * const opposite = [edge opposite];
        
        if (![edge isNull] && !opposite && ![opposite isNull] && opposite.face) {
            [faces addObject:opposite.face];
        }
    }];
    
    return faces;
}

- (NSArray *)boundaryEdges {
    return [_edges filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(SGHalfEdge *edge, NSDictionary *bindings) {
        return ![edge opposite];
    }]];
}

- (BOOL)isBoundaryFace {
    return [[self boundaryEdges] count] > 0;
}

- (BOOL)isEqual:(SGHalfEdgeFace *)object {
    return [_data isEqual:[object data]];
}


- (void)setEdges:(NSArray *)edges {
    NSMutableArray * const workArray = [_edges mutableCopy];
    
    [edges enumerateObjectsUsingBlock:^(SGHalfEdge *currentEdge, NSUInteger idx, BOOL *stop) {
        [self _replaceEdge:currentEdge atIndex:idx inEdgeArray:workArray];
    }];
    
    _edges = [workArray copy];
}

- (void)replaceEdge:(SGHalfEdge *)edge atIndex:(NSUInteger)idx {
    NSMutableArray * const workArray = [_edges mutableCopy];
    
    [self _replaceEdge:edge atIndex:idx inEdgeArray:workArray];
    
    _edges = [workArray copy];
}

- (void)_replaceEdge:(SGHalfEdge *)edge atIndex:(NSUInteger)idx inEdgeArray:(NSMutableArray *)edgeArray {
    const NSRange    edgeRange = NSMakeRange(0, [[self class] _numberOfEdges]);
    const NSUInteger prevIdx   = [[self class] _previousIndex:idx inRange:edgeRange];
    const NSUInteger nextIdx   = [[self class] _nextIndex:idx inRange:edgeRange];
    
    SGHalfEdge * const prev = edgeArray[prevIdx];
    SGHalfEdge * const next = edgeArray[nextIdx];
    
    [edge setPrevious:prev];
    [edge setNext:next];
    
    [prev setNext:edge];
    [next setPrevious:edge];
    
    edge.face = self;
    
    [edgeArray replaceObjectAtIndex:idx withObject:edge];
}

@end

//--------------------------------------------------------------------------------------------------------------------------------

@implementation SGHalfTriangle

+ (NSUInteger)_numberOfEdges {
    return 3;
}

@end

//--------------------------------------------------------------------------------------------------------------------------------

@implementation SGHalfQuad

+ (NSUInteger)_numberOfEdges {
    return 4;
}

@end
