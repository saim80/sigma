//
//  SGHalfEdge_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGHalfEdge.h"
#import "SGHalfEdgeFace.h"

@interface SGHalfEdge () {
@protected
    __weak SGHalfEdge *_next;
    __weak SGHalfEdge *_previous;
    __weak SGHalfEdge *_opposite;
    __weak SGHalfEdgeFace *_face;
    id _data;
}
@property (nonatomic, assign)   BOOL isVisited;
@property (nonatomic, readonly) BOOL isNull;

@end
