//
//  SGHalfEdge.h
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

@class SGHalfEdgeFace;

@interface SGHalfEdge : NSObject
@property (nonatomic, strong) id             data;
@property (nonatomic, weak)   SGHalfEdgeFace *face;
@property (nonatomic, weak)   SGHalfEdge     *opposite;
@property (nonatomic, weak)   SGHalfEdge     *next;
@property (nonatomic, weak)   SGHalfEdge     *previous;

- (instancetype)initWithPrevious:(SGHalfEdge *)previous
                            next:(SGHalfEdge *)next
                        opposite:(SGHalfEdge *)edge;

- (BOOL)isNull;

+ (SGHalfEdge *)nullEdge;

@end
