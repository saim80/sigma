//
//  SGHalfEdgeMesh2D_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGHalfEdgeMesh.h"
#import "SGHalfEdge_Private.h"
#import "SGHalfEdgeFace.h"

@interface SGHalfEdgeMesh ()
@property (nonatomic, strong) NSMutableArray *cachedBoundaryEdges;
@property (nonatomic, strong) NSMutableSet   *cachedBoundaryFaces;
@property (nonatomic, strong) NSMutableArray *allFaces;

@end
