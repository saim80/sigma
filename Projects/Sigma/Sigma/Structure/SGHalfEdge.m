//
//  SGHalfEdge.m
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGHalfEdge_Private.h"

@implementation SGHalfEdge

- (instancetype)initWithPrevious:(SGHalfEdge *)previous
                            next:(SGHalfEdge *)next
                        opposite:(SGHalfEdge *)edge {
    self = [super init];
    
    if (self) {
        _previous = previous;
        _next     = next;
        _opposite = edge;
    }
    
    return self;
}

- (SGHalfEdge *)opposite {
    return _opposite;
}

- (SGHalfEdge *)next {
    return _next;
}

- (SGHalfEdge *)previous {
    return _previous;
}

- (void)_setIsNull:(BOOL)isNull {
    _isNull = isNull;
}

+ (SGHalfEdge *)nullEdge {
    static SGHalfEdge *null;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        null = [SGHalfEdge new];
        
        [null _setIsNull:YES];
    });
    
    return null;
}

- (BOOL)isEqual:(SGHalfEdge *)object {
    return [_data isEqual:[object data]];
}

@end

