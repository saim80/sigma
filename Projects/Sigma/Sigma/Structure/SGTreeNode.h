//
//  SGTreeNode.h
//  SGApp
//
//  Created by Sangwoo Im on 4/11/13.
//  Copyright (c) 2013 Sangwoo Im. All rights reserved.
//

@interface SGTreeNode : NSObject <NSCopying, NSMutableCopying>

@property (nonatomic, weak, readonly) SGTreeNode *parent;

@property (nonatomic, assign) NSInteger tag;

- (id)initWithParent:(SGTreeNode *)parent children:(NSArray *)children;
- (NSArray *)children;

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGMutableTreeNode : SGTreeNode

- (void)willMoveToParent:(SGMutableTreeNode *)parent;
- (void)didMoveToParent;

- (void)addChild:(SGMutableTreeNode *)child;
- (void)removeFromParent;

- (void)insertChild:(SGMutableTreeNode *)child atIndex:(NSUInteger)index;

@end
