//
//  SGHalfEdgeFace.h
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

@class SGHalfEdgeMesh, SGHalfEdge;

@protocol SGHalfEdgeRelation <NSObject>
@required
+ (BOOL)isEdge:(SGHalfEdge *)edgeA oppositeOfEdge:(SGHalfEdge *)edgeB;
+ (BOOL)isEdge:(SGHalfEdge *)edgeA nextOfEdge:(SGHalfEdge *)edgeB;
+ (BOOL)isEdge:(SGHalfEdge *)edgeA previousOfEdge:(SGHalfEdge *)edgeB;

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGHalfEdgeFace : NSObject
@property (nonatomic, weak) id data;
@property (nonatomic, weak) SGHalfEdgeMesh *mesh;

+ (void)registerEdgeRelation:(Class)relation;
+ (void)unregisterEdgeRelation;

+ (Class)edgeRelation;

- (instancetype)initWithEdges:(NSArray *)edges;

+ (NSArray *)edgeLoop;

- (NSArray *)edges;
- (SGHalfEdge *)edgeAtIndex:(NSUInteger)idx;
- (BOOL)validate:(NSError *__autoreleasing*)outError;

- (NSArray *)adjacentFaces;
- (NSArray *)boundaryEdges;

- (BOOL)isBoundaryFace;

- (void)setEdges:(NSArray *)edges;
- (void)replaceEdge:(SGHalfEdge *)edge atIndex:(NSUInteger)idx;

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGHalfTriangle : SGHalfEdgeFace

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGHalfQuad : SGHalfEdgeFace

@end

//--------------------------------------------------------------------------------------------------------------------------------

typedef NS_ENUM(ushort, SGHalfEdgeFaceError) {
    SGHalfEdgeFaceBrokenForwardLink,
    SGHalfEdgeFaceBrokenBackwardLink
};