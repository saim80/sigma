//
//  SGHalfEdgeMesh2D.h
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGObject.h"

@class SGHalfEdge, SGHalfEdgeFace;

@interface SGHalfEdgeMesh : SGObject

- (instancetype)initWithMesh:(SGHalfEdgeMesh *)mesh;

- (NSArray *)boundaryEdges;
- (NSArray *)boundaryFaces;

- (NSArray *)faces;

- (BOOL)addFace:(SGHalfEdgeFace *)face;
- (BOOL)insertFace:(SGHalfEdgeFace *)face atIndex:(NSUInteger)idx;
- (void)removeFace:(SGHalfEdgeFace *)face;
- (void)removeFaceAtIndex:(NSUInteger)idx;

@end
