//
//  SGHalfEdgeFace2D_Private.h
//  SGApp
//
//  Created by Sangwoo Im on 3/30/14.
//  Copyright (c) 2014 Sangwoo Im. All rights reserved.
//

#import "SGHalfEdgeFace.h"
#import "SGHalfEdge_Private.h"

static Class SGHalfEdgeRelation;

@interface SGHalfEdgeFace () {
@protected
    SGHalfEdge *_head;
    NSArray    *_edges;
    
    __weak id _data;
    __weak SGHalfEdgeMesh *_mesh;
}

+ (NSUInteger)_numberOfEdges;

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGHalfTriangle ()

@end

//--------------------------------------------------------------------------------------------------------------------------------

@interface SGHalfQuad ()

@end
